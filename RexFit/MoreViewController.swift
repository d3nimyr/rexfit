//
//  MoreViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import StoreKit

class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKProductsRequestDelegate, SKPaymentTransactionObserver  {
    
    @IBOutlet weak var moreTable: UITableView! // moreTableView переименуй
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegating and datasourcing
        moreTable.dataSource = self
        moreTable.delegate = self
        
        SKPaymentQueue.default().add(self)
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x3478F6)
        
        moreCategories = [String](moreExerciseData.keys)
        moreTable.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0) // disable separator
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! MoreSettingTableViewCell
                cell.settingLabel.text = NSLocalizedString("settings", comment: "")
                cell.separatorInset = UIEdgeInsets.zero
                cell.selectionStyle = .none
                return cell
        } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell")
                cell?.separatorInset = UIEdgeInsets.zero
                cell?.textLabel?.text = NSLocalizedString("exercise", comment: "")
                return cell!
        } else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell")
                cell?.separatorInset = UIEdgeInsets.zero
                cell?.textLabel?.text = NSLocalizedString("categories", comment: "")
                return cell!
        } else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! MoreSettingTableViewCell
                cell.settingLabel.text = NSLocalizedString("premium", comment: "").uppercased()
                cell.separatorInset = UIEdgeInsets.zero
                cell.selectionStyle = .none
                return cell
        } else if indexPath.row == 4 {
                let cell = UITableViewCell()
                cell.textLabel?.text = NSLocalizedString("premium", comment: "")
                cell.separatorInset = UIEdgeInsets.zero
                return cell
        } else if indexPath.row == 5 {
                let cell = UITableViewCell()
                cell.textLabel?.text = NSLocalizedString("restore", comment: "")
                cell.separatorInset = .zero
                return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3 + (premiumOrdered ? 0 : 3)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MoreExercisesViewControllerID") as! MoreExerciseViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MoreCategoriesViewControllerID") as! MoreCategoriesViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 4:
                requestBuyPremium()
                //premium
            case 5:
                SKPaymentQueue.default().restoreCompletedTransactions()
            default: break
        }
     }
    
    func requestBuyPremium() {
        if (SKPaymentQueue.canMakePayments()) {
            
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: NSSet(object: productID) as! Set<String>)
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct) {
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    showAlert(title: "Success", message: "Premium has been purchased!")
                    set(success: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .failed:
                    print("Purchased Failed");
                    showAlert(title: "Error", message: "Error with ordering premium. Please, try again.")
                    set(success: false)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .restored:
                    print("Already Purchased");
                    showAlert(title: "Error", message: "Premium has been already purchased.")
                    set(success: false)
                    SKPaymentQueue.default().restoreCompletedTransactions()
                default:
                    break;
                }
            }
        }        
    }
    
    func set(success: Bool) {
        
        if defaults.bool(forKey: "premium") == false {
            defaults.set(success, forKey: "premium")
            defaults.synchronize()
        }
        
        premiumOrdered = success
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let count : Int = response.products.count
        if (count > 0) {
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == productID) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(product: validProduct);
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            print("nothing")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
