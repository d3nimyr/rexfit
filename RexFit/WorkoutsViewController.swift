//
//  WorkoutsViewController.swift
//  RexFit
//
//  Created by Hadevs on 19.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import StoreKit

public class Workout: NSObject {
    var name: String?
    var startDate: Date = Date()
    var endDate: Date?
    var exercises: [WorkoutExercise] = []
    var isCreated: Bool = false
    var bodyWeight: Double?
    var notes: String = ""
    var id: String!
    var isRepeated = false
    
    var sectionedDate: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "LLLL yyyy"
        let string = formatter.string(from: startDate)
        return string
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let workout = Workout()
        workout.name = name
        workout.startDate = startDate
        workout.endDate = endDate
        workout.exercises = exercises
        workout.isCreated = isCreated
        workout.bodyWeight = bodyWeight
        workout.notes = notes
        workout.id = id
        return workout
    }
    
    static func from(routine: RoutineWorkout) -> Workout {
        if routine.isCreated {
            let id = routine.id
            for workout in publicWorkouts where id == workout.id {
                return workout
            }
        } else {
            let workout = Workout(routine: routine)
            return workout
        }
        
        return Workout()
    }
    
    init(routine: RoutineWorkout) {
        self.name = routine.name
        self.notes = routine.notes
        self.exercises = routine.exercises
    }
    
    init(workout: Workout) {
        self.name = workout.name
        self.startDate = workout.startDate
        self.endDate = workout.endDate
        self.exercises = workout.exercises
        self.isCreated = workout.isCreated
        self.bodyWeight = workout.bodyWeight
        self.notes = workout.notes
        self.id = workout.id
    }
    
    override init() {}
}

public class WorkoutExercise: Exercise {
    var sets: [ExerciseSet] = [ExerciseSet(weight: 0, reps: 0)]
    
    func addSet(set: ExerciseSet) {
        sets.append(set)
    }
    
    init(exercise: Exercise) {
        super.init(name: exercise.name, category: exercise.category, type: exercise.type)
    }
    
    func removeSet(set: ExerciseSet) {
        guard let index = sets.index(of: set) else {
            return
        }
        
        sets.remove(at: index)
    }
}

class ExerciseSet: NSObject {
    var weight: Int
    var reps: Int
    
    init(weight: Int, reps: Int) {
        self.weight = weight
        self.reps = reps
        super.init()
    }
}

public var publicWorkouts: [Workout] = []

class WorkoutsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var workoutTableView: UITableView!
    
    private var sections: [String] = []
    
    private var workouts: [Workout] = [] {
        didSet {
            
            workouts = workouts.sorted(by: { (w1, w2) -> Bool in
                return w1.startDate.timeIntervalSince1970 >= w2.startDate.timeIntervalSince1970
            })
            
            sections = Array(Set(workouts.map({ (workout) -> String in
                let formatter = DateFormatter()
                formatter.dateFormat = "LLLL yyyy"
                let string = formatter.string(from: workout.startDate)
                return string
            })))
            
            defaults.set(workouts.toDictionaryArray(), forKey: "workouts")
            defaults.synchronize()
            
            loadWorkouts()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegating and datasourcing
        workoutTableView.delegate = self
        workoutTableView.dataSource = self
        
        print("public workoutss: \(publicWorkouts)")

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        workouts = publicWorkouts
        workoutTableView.reloadData()
    }
    
    @IBAction func createNewWorkoutButtonClicked(sender: UIBarButtonItem) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "workoutVC") as? WorkoutViewController else {
            return
        }
        
//        vc.lastVC = self
        
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    @IBAction func editButtonClicked(sender: UIBarButtonItem) {
        workoutTableView.setEditing(!workoutTableView.isEditing, animated: true)
        
        let button = workoutTableView.isEditing ? UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.editButtonClicked(sender:))) : UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editButtonClicked(sender:)))
        navigationItem.leftBarButtonItem = button
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let arrayOfSetions = workouts.filter { (workout) -> Bool in
            return sections[indexPath.section] == workout.sectionedDate
        }
        
        let workout = arrayOfSetions[indexPath.row]
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "workoutVC") as? WorkoutViewController else {
            return
        }
        
        vc.workout = workout
//        vc.lastVC = self
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let arrayOfSetions = workouts.filter { (workout) -> Bool in
                return sections[indexPath.section] == workout.sectionedDate
            }
            
            let workout = arrayOfSetions[indexPath.row]
            let indexToRemove = workouts.index(of: workout)
            
            workouts.remove(at: indexToRemove!)
            
            for exercise in workout.exercises {
                if let index = lastExercises.index(of: exercise) {
                    lastExercises.remove(at: index)
                }
            }
            
            if arrayOfSetions.count - 1 == 0 {
                tableView.beginUpdates()
                let indexSet = NSMutableIndexSet()
                indexSet.add(indexPath.section)
                tableView.deleteSections(indexSet as IndexSet, with: .automatic)
                tableView.endUpdates()
            } else {
                tableView.deleteRows(at: [indexPath], with: .right)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let arrayOfSetion = workouts.filter { (workout) -> Bool in
            return sections[indexPath.section] == workout.sectionedDate
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell", for: indexPath) as! WorkoutsTableViewCell
        cell.configure(workout: arrayOfSetion[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrayOfSetion = workouts.filter { (workout) -> Bool in
            return sections[section] == workout.sectionedDate
        }
        
        print("dota 2: \(arrayOfSetion)")
        return arrayOfSetion.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
