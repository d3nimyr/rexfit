//
//  AddCrunches.swift
//  RexFit
//
//  Created by Quaka on 19.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import StoreKit

struct tableData {
    var key: String
    var data: Any
}

class WorkoutViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SKPaymentTransactionObserver, SKProductsRequestDelegate {
    
    @IBOutlet weak var workoutTableView: UITableView!
    @IBOutlet weak var completeButton: UIBarButtonItem!
    
    var workout: Workout = Workout()
    var notesText = "" {
        didSet {
            workout.notes = notesText
        }
    }
    
    var fromHistory = false
    
    private var selectedTag: Int! // for tag in tableview textfields
    private var donePanel: DonePanel!
    private var datePicker: UIDatePicker!
    private var selectedStartDate = Date() {
        didSet {

            title = selectedStartDate.convertFormateToNormDateString(format:"E, dd MMMM")
            
            workout.startDate = selectedStartDate
            workoutTableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
        }
    }
    
    private var selectedEndDate: Date? {
        didSet {
            workout.endDate = selectedEndDate
            workoutTableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
    }
  
    private var addedTXIndexes: [Int] = [] {
        didSet {
            addedTXIndexes = Array(Set(addedTXIndexes)).sorted()
        }
    }
    
    private func getTXIndex(next: Bool, fromIndex index: Int) -> Int? {
        if next {
            let nextIndex = addedTXIndexes.index(of: index)! + 1
            return addedTXIndexes.contains(index: nextIndex) ? addedTXIndexes[nextIndex] : nil
        } else {
            let prevIndex = addedTXIndexes.index(of: index)! - 1
            return addedTXIndexes.contains(index: prevIndex) ? addedTXIndexes[prevIndex] : nil
        }
    }
    
    private var allData: [tableData] = []
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        completeButton.title = fromHistory ? NSLocalizedString("history", comment: "") : NSLocalizedString("complete", comment: "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        workoutTableView.backgroundColor = UIColor(netHex: 0xf1f1f1)
        workoutTableView.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
        
        workoutTableView.delegate = self
        workoutTableView.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
        
        title = !workout.isCreated ? Date().convertFormateToNormDateString(format: "E, d MMMM") : workout.startDate.convertFormateToNormDateString(format: "E, d MMMM")
        
        addNotificationsForKeyboard()
        
        selectedStartDate = workout.startDate
        selectedEndDate = workout.endDate
        notesText = workout.notes
        workout.bodyWeight = savedWeight
        
        workoutTableView.reloadData()
        workoutTableView.keyboardDismissMode = .onDrag
        
        appearDonePanel(withSelector: #selector(self.doneButtonClickedWhenKeyboardActive(sender:)))
        
        fillAllData(withExercise: nil)
        
        
    }
    
    private var isKeyboardOpened: Bool = false
 
    func keyboardChangeFrame(_ notification:Notification) {
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        donePanel.frame.origin.y = keyboardRect.origin.y - donePanel.frame.height
    }
    
//    func keyboardWillShow(_ notification:Notification) {
//        isKeyboardOpened = true
//
//        guard let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else {
//            return
//        }
//
//        let height = keyboardSize.height
//        let duration = Double(String(describing: notification.userInfo![UIKeyboardAnimationDurationUserInfoKey]!))
//
//        let curveOption = Int(String(describing: notification.userInfo![UIKeyboardAnimationCurveUserInfoKey]!))! << 16
//
//        UIView.animate(withDuration: duration!, delay: 0, options: [UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions(rawValue: UInt(curveOption))], animations: {
//            let edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: height + 230, right: 0)
//            self.workoutTableView.contentInset = edgeInsets
//            self.workoutTableView.scrollIndicatorInsets = edgeInsets
//        }, completion: nil)
//
//        donePanel.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height - donePanel.frame.height, width: self.view.frame.height, height: donePanel.frame.height)
////        }
//
//        donePanel.doneButton.removeTarget(nil, action: nil, for: .allEvents)
//
//        donePanel.doneButton.addTarget(self, action: #selector(self.doneButtonClickedWhenKeyboardActive(sender:)), for: .touchUpInside)
//    }
    
    func keyboardWillShow(_ notification:Notification) {
        isKeyboardOpened = true
        
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        workoutTableView.contentOffset.y += keyboardRect.height / 2
        donePanel.frame.origin.y = keyboardRect.origin.y - donePanel.frame.height
    }
    
    func keyboardDidHide(_ notification:Notification) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let updatedText = textFieldText.replacingCharacters(in: range, with: string)
        
        guard let indexPath = workoutTableView.indexPathForView(view: textField) else {
            return true
        }
        
        switch textField.tag {
            case 10:
                if !updatedText.isEmpty {
                    workout.name = updatedText
                }
            case 40:
                if !updatedText.isEmpty {
                    workout.bodyWeight = Double(updatedText)!
                    savedWeight = workout.bodyWeight!
                }
            default:
                if textField.tag % 10 == 2 {
                    let data = allData[indexPath.row - 7].data as! (set: ExerciseSet, exercise: WorkoutExercise)
                    let exercise = data.exercise
                    if !updatedText.isEmpty {
                        let reps = Int(updatedText)!
                        workout.exercises[workout.exercises.index(of: exercise)!].sets[workout.exercises[workout.exercises.index(of: exercise)!].sets.index(of: data.set)!].reps = reps
                    }
                } else if textField.tag % 10 == 1 {
                    let data = allData[indexPath.row - 7].data as! (set: ExerciseSet, exercise: WorkoutExercise)
                    let exercise = data.exercise
                    if !updatedText.isEmpty {
                        let weight = Int(updatedText)!
                        workout.exercises[workout.exercises.index(of: exercise)!].sets[workout.exercises[workout.exercises.index(of: exercise)!].sets.index(of: data.set)!].weight = weight
                    }
                }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTag = textField.tag
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        delay(delay: 0.4) {
//            let pointInTable: CGPoint = textField.superview!.convert(textField.frame.origin, to: self.workoutTableView)
//            var contentOffset = self.workoutTableView.contentOffset
//            contentOffset.y = pointInTable.y - 220 + (45 * CGFloat(self.workout.exercises.count))
//            self.workoutTableView.setContentOffset(contentOffset, animated: true)
//            self.workoutTableView.frame.size.height = self.view.frame.height * 0.6
//        }
        
        return true
    }
    
    func fillAllData(withExercise choosedExercise: WorkoutExercise?) {
        allData.removeAll()
        
        var index: Int!
        for exercise in workout.exercises {
            allData.append(tableData(key: "name", data: exercise))
            
            if exercise.sets.count == 0 {
                allData.append(tableData(key: "set", data: (set: ExerciseSet(weight: 0, reps: 0), exercise: exercise)))
            } else {
                for set in exercise.sets {
                    allData.append(tableData(key: "set", data: (set: set, exercise: exercise)))
                }
            }
            
            allData.append(tableData(key: "addSet", data: exercise))
            allData.append(tableData(key: "empty", data: 0))
            
            if let exer = choosedExercise, exercise == exer {
                var i = 0
                
                self.allData.forEach({ (data) in
                    if data.data is (set: ExerciseSet, exercise: WorkoutExercise) && (data.data as! (set: ExerciseSet, exercise: WorkoutExercise)).exercise == exer {
                        if index == nil {
                            index = i
                        }
                    }
                    
                    i += 1
                })
            }
        }
        
        workoutTableView.reloadData()
        
        if index != nil, let cell = workoutTableView.cellForRow(at: IndexPath(row: index + 7, section: 0)) as? WorkoutCharacterTableViewCell {
            cell.weightField.becomeFirstResponder()
        } else {
            workoutTableView.scrollToBottom(animated: true)
            workoutTableView.scrollToRow(at: (workoutTableView.numberOfRows(inSection: 0) - 1).toIndexPath(), at: .bottom, animated: true)
            
            if index != nil, let cell = workoutTableView.cellForRow(at: IndexPath(row: index + 7, section: 0)) as? WorkoutCharacterTableViewCell {
                cell.weightField.becomeFirstResponder()
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        workoutTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
        hideDonePanel()
    }
    
    func addNotificationsForKeyboard() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidHide(_:)),
                                               name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardChangedFrame(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }

    func keyboardWillHide(notification: NSNotification) {
        
        self.hidePicker()
        self.hideDonePanel()
        
        workoutTableView.frame = view.bounds
        workoutTableView.frame.origin.y = navigationController!.navigationBar.frame.maxY
        workoutTableView.frame.size.height = workoutTableView.frame.size.height - navigationController!.navigationBar.frame.height - 20

        let duration = Double(String(describing: notification.userInfo![UIKeyboardAnimationDurationUserInfoKey]!))
        
        let curveOption = Int(String(describing: notification.userInfo![UIKeyboardAnimationCurveUserInfoKey]!))! << 16
        
        UIView.animate(withDuration: duration!, delay: 0, options: [UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions(rawValue: UInt(curveOption))], animations: {
            self.workoutTableView.contentInset = UIEdgeInsets.zero
            self.workoutTableView.scrollIndicatorInsets = .zero
        }, completion: nil)
        
    }
    
    func keyboardChangedFrame(notification: NSNotification) {
//        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue, let panel = donePanel, isKeyboardOpened else {
//            return
//        }
//
//        donePanel.frame.origin.y = keyboardRect.origin.y - donePanel.frame.height
    }
    
    
    func doneButtonClickedWhenKeyboardActive(sender: UIButton) {
        view.endEditing(true)
        hideDonePanel()
        hidePicker()
    }
    
    @IBAction func completeButtonClicked(sender: UIBarButtonItem) {
        
        save()
        
        smartBack()
    }
    
    fileprivate func save() {
        workout.isRepeated = false
        
        if !workout.isCreated {
            workout.startDate = selectedStartDate
            
            if let endDate = selectedEndDate {
                workout.endDate = endDate
            }
            
            workout.id = ID()
            workout.isCreated = true
            
            publicWorkouts.append(workout)
        } else {
            
            for work in publicWorkouts where work.id == self.workout.id {
                publicWorkouts[publicWorkouts.index(of: work)!].exercises.forEach({ (ex) in
                    ex.sets.forEach({ (set) in
                        print("Ses: \(set.reps), \(set.weight)")
                    })
                })
                
                publicWorkouts[publicWorkouts.index(of: work)!] = self.workout
                break
            }
        }
        
        for exercise in workout.exercises {
            replaceExerciseInLastExercises(withExercise: exercise)
        }
    }
    
    fileprivate func saveWorkoutToRoutines() {
        if routines.count == 0 || premiumOrdered {
            workout.id = workout.id ?? ID()
            let routine = RoutineWorkout(from: workout)
            routines.insert(routine, at: 0)
        } else {
            showAlert(title: NSLocalizedString("error", comment: ""), message: L.s("shouldBuyPremium"))
        }
    }
    
    @IBAction func repeatButtonClicked(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let repeatAction = UIAlertAction(title: NSLocalizedString("repeat", comment: ""), style: .default) { (_) in
            self.repeatWorkout()
        }
        
        let saveAsRoutineAction = UIAlertAction(title: NSLocalizedString("saveAsRoutine", comment: ""), style: .default) { (_) in
            self.saveWorkoutToRoutines()
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
        
        alertController.addAction(repeatAction)
        alertController.addAction(saveAsRoutineAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    private func repeatWorkout() {
        
        let copyWorkout = self.addCopyWorkout()
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "workoutVC") as? WorkoutViewController else {
            return
        }
        
        vc.workout = copyWorkout
        
        if isModal() {
            dismiss(animated: true) {
                if let window = UIApplication.shared.keyWindow, let rootVC = window.rootViewController {
                    rootVC.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
        } else {
            self.navigationController!.popViewController(animated: true, completion: {
                if let window = UIApplication.shared.keyWindow, let rootVC = window.rootViewController {
                    rootVC.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            })
        }
    }
        
    private func addCopyWorkout() -> Workout {
        let copyWorkout = Workout()
        copyWorkout.name = workout.name
        copyWorkout.exercises = workout.exercises
        copyWorkout.bodyWeight = workout.bodyWeight
        copyWorkout.notes = workout.notes
        copyWorkout.startDate = Date()
        copyWorkout.endDate = nil
        copyWorkout.isRepeated = true
        copyWorkout.isCreated = false
        return copyWorkout
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 2 {
            appearPicker(withSelector: #selector(startDateChanged(sender:)))
        } else if indexPath.row == 3 {
            selectedEndDate = Date()
            appearPicker(withSelector: #selector(endDateChanged(sender:)))
        } else if indexPath.row == 5 {
            let textVC = TextViewViewController()
            navigationController?.pushViewController(textVC, animated: true)
        } else if indexPath.row == 7 + allData.count {
            let vc = SelectCategoryTableViewController()
            let comp: ((Exercise) -> Void) = {
                exercise in
                
                let workoutExercise = WorkoutExercise(exercise: exercise)
                if !self.workout.exercises.contains(workoutExercise) {
                    self.workout.exercises.append(workoutExercise)
                } else {
                    self.showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("addExerciseError", comment: ""))
                }
                
               self.fillAllData(withExercise: workoutExercise)
            }
            
            vc.completion = comp
            let nc = UINavigationController(rootViewController: vc)
            
            present(nc, animated: true, completion: nil)
        } else if workout.exercises.count > 0 && indexPath.row > 6 && indexPath.row < 7 + allData.count && allData[indexPath.row - 7].key == "addSet" {
            let exercise = allData[indexPath.row - 7].data as! WorkoutExercise
            exercise.sets.append(exercise.sets.last!)
            workout.exercises[workout.exercises.index(of: exercise)!].sets = exercise.sets
            fillAllData(withExercise: nil)
            
            if let cell = tableView.cellForRow(at: indexPath) as? WorkoutCharacterTableViewCell {
                cell.weightField.becomeFirstResponder()
            }
        } else if workout.exercises.count > 0 && indexPath.row > 6 && indexPath.row < 7 + allData.count && allData[indexPath.row - 7].key == "set" {
            if let cell = tableView.cellForRow(at: indexPath) as? WorkoutCharacterTableViewCell {
                cell.weightField.becomeFirstResponder()
            }
        }
    }
    
    func endDateChanged(sender: UIDatePicker) {
        selectedEndDate = sender.date
    }
    
    func startDateChanged(sender: UIDatePicker) {
        selectedStartDate = sender.date
    }
    
    private func saveLastEditedExercise(exercise: WorkoutExercise) {
        defaults.set(exercise.toDictionary(), forKey: "lastExercise")
        defaults.synchronize()
    }
    
    private func loadLastEditedExercise() -> WorkoutExercise? {
        return defaults.object(forKey: "lastExercise") as? WorkoutExercise
    }
    
    func appearDonePanel(withSelector selector: Selector) {
        if donePanel == nil || donePanel != nil && !view.subviews.contains(donePanel) {
            let frame = CGRect(x: 0, y: view.frame.maxY, width: view.frame.width, height: 44)
            donePanel = DonePanel(frame: frame)
            
            donePanel.doneButton.removeTarget(nil, action: nil, for: .allEvents)
            
            donePanel.doneButton.addTarget(self, action: selector, for: .touchUpInside)
            donePanel.leftButton.addTarget(self, action: #selector(self.leftButtonClicked(sender:)), for: .touchUpInside)
            donePanel.rightButton.addTarget(self, action: #selector(self.rightButtonClicked(sender:)), for: .touchUpInside)
            
            view.addSubview(donePanel)
        }
    }
    
    func leftButtonClicked(sender: UIButton) {
        
        guard let tag = selectedTag, let nextTag = getTXIndex(next: false, fromIndex: tag), let textField = workoutTableView.viewWithTag(nextTag) as? UITextField else {
            view.endEditing(true)
            return
        }
        
        textField.becomeFirstResponder()
    }
    
    func rightButtonClicked(sender: UIButton) {
        
        guard let tag = selectedTag, let nextTag = getTXIndex(next: true, fromIndex: tag), let textField = workoutTableView.viewWithTag(nextTag) as? UITextField else {
            view.endEditing(true)
            return
        }
        
        textField.becomeFirstResponder()
    }
    
    func hideDonePanel() {
        if donePanel != nil {
            UIView.animate(withDuration: 0.25, animations: {
                self.donePanel.frame.origin.y = 1500
            })
        }
    }
    
    func doneButtonClickedWhenStartDatePickerActive(sender: UIButton) {
        view.endEditing(true)
        hidePicker()
        hideDonePanel()
    }
    
    func appearPicker(withSelector selector: Selector) {
        
        resignAllTextFields()
//        hideDonePanel()
        
        datePicker = UIDatePicker()
        datePicker.frame.size = CGSize(width: view.frame.width, height: view.frame.height * 0.3)
        datePicker.frame.origin = CGPoint(x: 0, y: 1500)
        datePicker.setDate(Date(), animated: true)
        datePicker.addTarget(self, action: selector, for: .valueChanged)
        datePicker.backgroundColor = UIColor(netHex: 0xf1f1f1)
        view.addSubview(datePicker)
        
        if donePanel == nil {
            appearDonePanel(withSelector: #selector(self.doneButtonClickedWhenStartDatePickerActive(sender:)))
        } else {
            donePanel.doneButton.removeTarget(nil, action: nil, for: .allEvents)
            donePanel.doneButton.addTarget(self, action: #selector(self.doneButtonClickedWhenStartDatePickerActive(sender:)), for: .touchUpInside)
        }
        
        UIView.animate(withDuration: 0.2) {
            self.datePicker.frame.origin.y = self.view.frame.maxY - self.datePicker.frame.height
            self.donePanel.frame.origin.y = self.datePicker.frame.origin.y - self.donePanel.frame.height
        }
    }
    
    func hidePicker() {
        
        for picker in view.subviews where picker is UIDatePicker {
            UIView.animate(withDuration: 0.25, animations: {
                picker.frame.origin.y = 1500
            }) { (true) in
                picker.removeFromSuperview()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            if allData[indexPath.row - 7].key == "set" {
            
                let data = allData[indexPath.row - 7].data as! (set: ExerciseSet, exercise: WorkoutExercise)
                
                for exercise in workout.exercises {
                    if let index = lastExercises.index(of: exercise) {
                        lastExercises.remove(at: index)
                    }
                }
                
                self.workout.exercises[self.workout.exercises.index(of: data.exercise)!].removeSet(set: data.set)
                
                if self.workout.exercises[self.workout.exercises.index(of: data.exercise)!].sets.count == 0 {
                    self.workout.exercises.remove(at: self.workout.exercises.index(of: data.exercise)!)
                }
                
            } else if allData[indexPath.row - 7].key == "name" {
                let exercise = allData[indexPath.row - 7].data as! WorkoutExercise
                self.workout.exercises.remove(at: self.workout.exercises.index(of: exercise)!)
            }
            
            fillAllData(withExercise: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return workout.exercises.count > 0 && indexPath.row > 6 && indexPath.row < 7 + allData.count && (allData[indexPath.row - 7].key == "set" || allData[indexPath.row - 7].key == "name")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell", for: indexPath) as! WorkoutFieldTableViewCell
            cell.workoutTextField.placeholder = NSLocalizedString("workoutName", comment: "")
            cell.workoutTextField.keyboardType = .default
            cell.workoutTextField.autocorrectionType = .no
            cell.workoutTextField.tag = indexPath.row * 10
            addedTXIndexes.append(indexPath.row * 10)
            cell.workoutTextField.delegate = self
            cell.workoutTextField.text = workout.name
            cell.workoutTextField.autocapitalizationType = .words
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! WorkoutSettingTableViewCell
            cell.settingTextField.text = selectedStartDate.convertFormateToNormDateString(format:"E, dd MMMM, HH:mm")
            cell.settingTextField.isUserInteractionEnabled = false
            cell.selectionStyle = .none
            cell.settingLabel.text = NSLocalizedString("startTime", comment: "")
            cell.settingLabel?.textColor = UIColor.black
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! WorkoutSettingTableViewCell
            cell.settingTextField.placeholder = "-"
            cell.selectionStyle = .none
            cell.settingLabel.text = NSLocalizedString("endTime", comment: "")
            cell.settingTextField.text = selectedEndDate == nil ? nil : selectedEndDate!.convertFormateToNormDateString(format:"E, dd MMMM, HH:mm")
            cell.settingTextField.isUserInteractionEnabled = false
            cell.settingLabel?.textColor = UIColor.black
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! WorkoutSettingTableViewCell
            cell.settingTextField.placeholder = savedWeight == nil ? "0.0" : "\(savedWeight!)"
            cell.settingTextField.keyboardType = .decimalPad
            cell.settingTextField.placeholder = workout.bodyWeight == nil ? nil : "\(workout.bodyWeight!)"
            cell.selectionStyle = .none
            cell.settingLabel.text = NSLocalizedString("bodyWeight", comment: "")
            cell.settingLabel?.textColor = UIColor.black
            cell.settingTextField.tag = indexPath.row * 10
            addedTXIndexes.append(indexPath.row * 10)
            cell.settingTextField.delegate = self
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell", for: indexPath) as! WorkoutFieldTableViewCell
            cell.selectionStyle = .none
            cell.workoutTextField.placeholder = NSLocalizedString("notes", comment: "")
            cell.accessoryType = .disclosureIndicator
            cell.workoutTextField.isEnabled = false
            cell.workoutTextField.text = notesText.isEmpty ? nil : "\(notesText)"
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 6 {
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell
        } else if workout.exercises.count > 0 && indexPath.row > 6 && indexPath.row < 7 + allData.count {
            
            // тут динамические целлы С эксерсайзами
            let data = allData[indexPath.row - 7]
            
            switch data.key {
            case "name":
                let cell = tableView.dequeueReusableCell(withIdentifier: "practiceCell", for: indexPath) as! WorkoutPracticeTableViewCell
                cell.exercise = data.data as! WorkoutExercise
                cell.clickedButtonPractice.addTarget(self, action: #selector(self.practiceButtonClicked(sender:)), for: .touchUpInside)
                cell.practiceTextField.isEnabled = false
                return cell
            case "set":
                let cell = tableView.dequeueReusableCell(withIdentifier: "characterCell", for: indexPath) as! WorkoutCharacterTableViewCell
                cell.workout = self.workout
                cell.repsField.delegate = self
                
                cell.weightField.tag = indexPath.row * 10 + 1
                cell.repsField.tag = indexPath.row * 10 + 2
                
                addedTXIndexes.append(indexPath.row * 10 + 1)
                addedTXIndexes.append(indexPath.row * 10 + 2)
                
                cell.weightField.delegate = self
                
                cell.clickedButtonCharacter.addTarget(self, action: #selector(self.setButtonClicked(sender:)), for: .touchUpInside)
                cell.set = (data.data as! (set: ExerciseSet, exercise: WorkoutExercise)).set
                cell.exercise = (data.data as! (set: ExerciseSet, exercise: WorkoutExercise)).exercise
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                return cell
            case "addSet":
                let cell = tableView.dequeueReusableCell(withIdentifier: "addSetCell", for: indexPath) as! WorkoutAddSetTableViewCell
                cell.historyButton.addTarget(self, action: #selector(self.historyButtonClicked(sender:)), for: .touchUpInside)
                cell.historyButton.isHidden = true
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                return cell
            case "empty":
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                return cell
            default:
                return UITableViewCell()
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! WorkoutSettingTableViewCell
            cell.settingTextField.placeholder = ""
            cell.settingLabel.text = NSLocalizedString("addExercise", comment: "")
            cell.settingTextField.isUserInteractionEnabled = false
            cell.settingTextField.text = ""
            cell.settingLabel?.textColor = UIColor(netHex: 0x3478F6)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell
        }
    }
    
    func historyButtonClicked(sender: UIButton) {
        guard let indexPath = workoutTableView.indexPathForView(view: sender) else {
            return
        }
        
        let data = allData[indexPath.row - 7]
        
        if data.key == "addSet" {
            let exercise = data.data as! WorkoutExercise
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "historyVC") as? HistoryViewController else {
                return
            }
            
            vc.selectedExercise = exercise
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setButtonClicked(sender: UIButton) {
        guard let indexPath = workoutTableView.indexPathForView(view: sender) else {
            return
        }
        
        let data = allData[indexPath.row - 7].data as! (set: ExerciseSet, exercise: WorkoutExercise)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .destructive) { (_) in
            self.workout.exercises[self.workout.exercises.index(of: data.exercise)!].removeSet(set: data.set)
            if self.workout.exercises[self.workout.exercises.index(of: data.exercise)!].sets.count == 0 {
                self.workout.exercises.remove(at: self.workout.exercises.index(of: data.exercise)!)
            }
            
            self.fillAllData(withExercise: nil)
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func practiceButtonClicked(sender: UIButton) {
        guard let indexPath = workoutTableView.indexPathForView(view: sender) else {
            return
        }
        
        let data = allData[indexPath.row - 7].data as! WorkoutExercise
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let infoAction = UIAlertAction(title: NSLocalizedString("exerciseInfo", comment: ""), style: .default) { (_) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "singleExerciseViewControllerID") as! SingleExerciseViewController
            
            guard let defaultExercise = data.toDefaultExercise() else {
                self.showAlert(title: NSLocalizedString("error", comment: ""), message: "Selected exercise is not default. Please, select another one.")
                return
            }
            
            if moreExerciseData[data.category]!.sorted(by: { (ex1, ex2) -> Bool in
                return ex2.name > ex1.name
            }).index(of: data)! > 2 && !premiumOrdered {
                let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("premiumAlert", comment: ""), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Buy", style: .default, handler: { (_) in
                    self.requestBuyPremium()
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                
                alertController.addAction(yesAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            
            vc.selectedexercise = defaultExercise
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let historyAction = UIAlertAction(title: NSLocalizedString("history", comment: ""), style: .default) { (_) in
            guard let indexPath = self.workoutTableView.indexPathForView(view: sender) else {
                return
            }
            
            let data = self.allData[indexPath.row - 7].data as! WorkoutExercise
            
            let exercise = data
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "historyVC") as? HistoryViewController else {
                return
            }
            
            vc.selectedExercise = exercise
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let replaceAction = UIAlertAction(title: NSLocalizedString("replace", comment: ""), style: .default) { (_) in
            let vc = SelectCategoryTableViewController()
            let comp: ((Exercise) -> Void) = {
                exercise in
                let index = self.workout.exercises.index(of: data)
                let newExercise = WorkoutExercise(exercise: exercise)
                newExercise.sets = self.workout.exercises[index!].sets
                self.workout.exercises[index!] = newExercise
                
                self.fillAllData(withExercise: nil)
            }
            
            vc.completion = comp
            let nc = UINavigationController(rootViewController: vc)
            
            self.present(nc, animated: true, completion: nil)
        }
        

        let moveAction = UIAlertAction(title: NSLocalizedString("move", comment: ""), style: .default) { (_) in
            // move action
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "editorOfExercisesVC") as! EditExercisesViewController
            vc.workout = self.workout
            vc.completion = {
                self.fillAllData(withExercise: nil)
            }
            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
        }
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .destructive) { (_) in
            let index = self.workout.exercises.index(of: data)
            self.workout.exercises.remove(at: index!)
            self.fillAllData(withExercise: nil)
            
            for exercise in self.workout.exercises {
                if let index = lastExercises.index(of: exercise) {
                    lastExercises.remove(at: index)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
        
        alertController.addAction(infoAction)
        alertController.addAction(historyAction)
        alertController.addAction(replaceAction)
        alertController.addAction(moveAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func requestBuyPremium() {
        if (SKPaymentQueue.canMakePayments()) {
            
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: NSSet(object: productID) as! Set<String>)
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct) {
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    showAlert(title: "Success", message: "Premium has been purchased!")
                    set(success: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .failed:
                    print("Purchased Failed");
                    showAlert(title: "Error", message: "Error with ordering premium. Please, try again.")
                    set(success: false)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .restored:
                    print("Already Purchased");
                    showAlert(title: "Error", message: "Premium has been already purchased.")
                    set(success: false)
                    SKPaymentQueue.default().restoreCompletedTransactions()
                default:
                    break;
                }
            }
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let count : Int = response.products.count
        if (count > 0) {
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == productID) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(product: validProduct);
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            print("nothing")
        }
    }

    
    func set(success: Bool) {
        
        if defaults.bool(forKey: "premium") == false {
            defaults.set(success, forKey: "premium")
            defaults.synchronize()
        }
        
        premiumOrdered = success
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if workout.exercises.count > 0 && indexPath.row > 6 && indexPath.row < 7 + allData.count && allData[indexPath.row - 7].key == "empty" {
            return 22
        } else {
            return indexPath.row == 6 ? 22 : 44
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8 + allData.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension UINavigationController {
    func pushViewController(viewController: UIViewController, animated: Bool, completion: @escaping () -> ()) {
        pushViewController(viewController, animated: animated)
        
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
    
    func popViewController(animated: Bool, completion: @escaping () -> ()) {
        popViewController(animated: animated)
        
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}

extension Int {
    func toIndexPath() -> IndexPath {
        return IndexPath(row: self, section: 0)
    }
}

extension UITableView {
    func scrollToBottom(animated: Bool) {
        let y = contentSize.height - frame.size.height
        setContentOffset(CGPoint(x: 0, y: (y<0) ? 0 : y), animated: animated)
    }
}
