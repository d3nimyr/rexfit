//
//  SelectExerciseTableViewCell.swift
//  RexFit
//
//  Created by Hadevs on 20.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class SelectExerciseTableViewCell: UITableViewCell {

    var infoButton: UIButton!
    var label: UILabel!
    
    func initInfoButton() {
        infoButton = UIButton(type: .infoLight)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(infoButton)
        
        let height = NSLayoutConstraint(item: infoButton, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0.8, constant: 0)
        let width = NSLayoutConstraint(item: infoButton, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0.8, constant: 0)
        let trailing = NSLayoutConstraint(item: infoButton, attribute: .trailingMargin, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -32)
        let centerY = NSLayoutConstraint(item: infoButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        addConstraints([height, width, trailing, centerY])
    }
    
    func initLabel() {
        label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 17.0)
        
        addSubview(label)
        
        let top = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: label, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: label, attribute: .leadingMargin, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 16)
        let trailing = NSLayoutConstraint(item: label, attribute: .trailingMargin, relatedBy: .equal, toItem: infoButton, attribute: .leading, multiplier: 1, constant: 8)
        
        addConstraints([top, bottom, leading, trailing])
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initInfoButton()
        initLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
