//
//  DetailTableViewCell.swift
//  RexFit
//
//  Created by Hadevs on 26/09/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rightLabel.text = NSLocalizedString("sets", comment: "").uppercased()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
