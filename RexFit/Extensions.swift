//
//  Extensions.swift
//  RexFit
//
//  Created by Никита on 16.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

extension String {
    func convertToEnum() -> ExerciseType {
        switch self.lowercased() {
        case "strength":
            return .strength
        case "cardio":
            return .cardio
        case "other":
            return .other
        default:
            return .strength
        }
    }
}

extension ExerciseType {
    func toString() -> String {
        switch self {
            case .cardio: return "cardio"
            case .other: return "other"
            case .strength: return "strength"
        }
        
    }
}

extension Array where Element: NSDictionary {
    func toWorkoutsArray() -> [Workout] {
        let array : [[String: Any?]] = self as! [[String : Any?]]
        
        var returnedArray: [Workout] = []
        
        array.forEach { (workoutDict) in
            let workout = Workout()
            workout.name = workoutDict["name"] as? String
            workout.startDate = workoutDict["startDate"] as! Date
            workout.endDate = workoutDict["endDate"] as? Date
            workout.exercises = (workoutDict["exercises"] as! [[String: Any?]]).map({ (dict) -> WorkoutExercise in
                
                let exercise = Exercise(name: dict["name"] as! String, category: dict["category"] as! String, type: (dict["type"] as! String).convertToEnum())
                let workoutExercise = WorkoutExercise(exercise: exercise)
                workoutExercise.sets = (dict["sets"] as! [[String: Int]]).map({ (setDict) -> ExerciseSet in
                    return ExerciseSet(weight: setDict["weight"]!, reps: setDict["reps"]!)
                })
                
                return workoutExercise
            })
            
            workout.isCreated = workoutDict["isCreated"] as! Bool
            workout.isRepeated = (workoutDict["isRepeated"] as? Bool) ?? false
            workout.bodyWeight = workoutDict["bodyWeight"] as? Double
            workout.notes = workoutDict["notes"] as! String
            workout.id = workoutDict["id"] as! String
            returnedArray.append(workout)
        }
        
        return returnedArray
    }
}

extension Exercise {
    func toDefaultExercise() -> DefaultExercise? {
        let name = self.name
        let category = self.category
        let type = self.type
        
        let allDefaultExercises: [DefaultExercise] = exerciseData.values.flatMap{$0}
        for exercise in allDefaultExercises where exercise.name == name && exercise.category == category && exercise.type == type {
            return exercise
        }
        
        return nil
    }
}

extension WorkoutExercise {
    func toDictionary() -> [String: Any] {
        var setsDict: [[String: Int]] = []
        let exercise = self
        
        exercise.sets.forEach({ (set) in
            let setDict = [
                "weight": set.weight,
                "reps": set.reps
            ]
            
            print("PEPS:")
            print(set.weight, set.reps)
            setsDict.append(setDict)
        })
        
        let exerciseDict: [String: Any?] = [
            "name": exercise.name,
            "category": exercise.category,
            "type": exercise.type.toString(),
            "sets": setsDict
        ]
        
        return exerciseDict
    }
}

extension NSObject {
    func replaceExerciseInLastExercises(withExercise exercise: WorkoutExercise) {
        
        exercise.sets.forEach { (set) in
            if set.reps == 0 || set.weight == 0 {
                return
            }
        }
        
        
        if lastExercises.contains(exercise) {
            for lastExercise in lastExercises where lastExercise == exercise {
                lastExercises[lastExercises.index(of: lastExercise)!] = exercise
            }
        } else {
            lastExercises.append(exercise)
        }
    }
    
    func getLastExercise(fromExercise exercise: WorkoutExercise) -> WorkoutExercise? {
        for lastExercise in lastExercises where lastExercise == exercise {
           return lastExercise
        }
        
        return nil
    }
}

extension Dictionary {
    func toWorkoutExercise() -> WorkoutExercise {
        let dict = self as! [String: Any]
        let exercise = Exercise(name: dict["name"] as! String, category: dict["category"] as! String, type: (dict["type"] as! String).convertToEnum())
        let workoutExercise = WorkoutExercise(exercise: exercise)
        workoutExercise.sets = (dict["sets"] as! [[String: Int]]).map({ (setDict) -> ExerciseSet in
            return ExerciseSet(weight: setDict["weight"]!, reps: setDict["reps"]!)
        })
        
        return workoutExercise
    }
}

extension Array where Element: Workout {
    func toDictionaryArray() -> [[String: Any]] {
        
        var returnedArray: [[String: Any]] = []
        let array = self as [Workout]
        
        array.forEach { (workout) in
            
            var exercisesArray: [[String: Any?]] = []
            
            workout.exercises.forEach({ (exercise) in
                
                var setsDict: [[String: Int]] = []
                
                exercise.sets.forEach({ (set) in
                    let setDict = [
                    "weight": set.weight,
                    "reps": set.reps
                    ]
                    
                    setsDict.append(setDict)
                })
                
                let exerciseDict: [String: Any?] = [
                    "name": exercise.name,
                    "category": exercise.category,
                    "type": exercise.type.toString(),
                    "sets": setsDict
                ]
                
                exercisesArray.append(exerciseDict)
            })
            
            
            let workoutDict: [String: Any?] = [
                "name": workout.name ?? "",
                "startDate": workout.startDate,
                "endDate": workout.endDate ?? workout.startDate,
                "exercises": exercisesArray,
                "isCreated": workout.isCreated,
                "isRepeated": workout.isRepeated,
                "bodyWeight": workout.bodyWeight ?? 0,
                "id": workout.id,
                "notes": workout.notes
            ]
            
            returnedArray.append(workoutDict)
            
        }
        
        return returnedArray
    }
}
