//
//  HistoryViewController.swift
//  RexFit
//
//  Created by Quaka on 01.08.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

struct HistoryStruct {
    var exercise: WorkoutExercise!
    var workout: Workout!
}

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var historyTableView: UITableView!
    
    var selectedExercise: WorkoutExercise!
    
    private var allData: [HistoryStruct] = []
    private var dates: [Date] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        historyTableView.delegate   = self
        historyTableView.dataSource = self
        
        loadExercises()
        historyTableView.reloadData()
        
        title = NSLocalizedString("history", comment: "")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initBackButton()
    }
    
    func initBackButton() {
        let backButton = UIBarButtonItem(title: NSLocalizedString("done", comment: "").capitalized, style: .plain, target: self, action: #selector(self.backButtonClicked(sender:)))
        navigationItem.leftBarButtonItem = backButton
    }
    
    func backButtonClicked(sender: UIBarButtonItem) {
        smartBack()
    }
    
    private func loadExercises() {
        var returnedArray: [HistoryStruct] = []
        for workout in publicWorkouts {
            for exercise in workout.exercises where exercise == selectedExercise {
                
                returnedArray.append(HistoryStruct(exercise: exercise, workout: workout))
                dates.append(workout.startDate)
            }
        }
        
        allData = returnedArray
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let workout = allData[indexPath.row].workout
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "workoutVC") as? WorkoutViewController else {
            return
        }
        
        vc.workout = workout!
        vc.fromHistory = true
//        vc.lastVC = (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 2] as! WorkoutViewController).lastVC
        navigationController?.pushViewController(vc, animated: true)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryTableViewCell
        
        let data = allData[indexPath.row]
        
        cell.dateHistoryLabel.text = dates[indexPath.row].convertFormateToNormDateString(format: "dd MMMM yyy г.")
        cell.setsHistoryLabel.text = "Sets"
        cell.charasteristicHistoryLabel.text = data.exercise.sets.filter({ (set) -> Bool in
            return set.reps > 0 && set.weight > 0
        }).map({ (set) -> String in
            return "\(set.weight)x\(set.reps)"
        }).joined(separator: ", ")
        
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allData.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
