//
//  EditExercisesViewController.swift
//  RexFit
//
//  Created by Hadevs on 23.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class EditExercisesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var editExercisesTableView: UITableView!
    
    var workout: Workout!
    
    var completion: (() -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // datasourcing and delegating
        editExercisesTableView.delegate = self
        editExercisesTableView.dataSource = self
        editExercisesTableView.setEditing(true, animated: true)
        editExercisesTableView.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            workout.exercises.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .right)
            if workout.exercises.count == 0 {
                completion()
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let t = workout.exercises[destinationIndexPath.row]
        workout.exercises[destinationIndexPath.row] = workout.exercises[sourceIndexPath.row]
        workout.exercises[sourceIndexPath.row] = t
    }
    
    func doneButtonClicked(sender: UIBarButtonItem) {
        completion()
        dismiss(animated: true, completion: nil)
    }
    
    func addButtonClicked(sender: UIBarButtonItem) {
        let vc = SelectCategoryTableViewController()
        let comp: ((Exercise) -> Void) = {
            exercise in
            
            self.workout.exercises.append(WorkoutExercise(exercise: exercise))
            self.editExercisesTableView.reloadData()
        }
        vc.completion = comp
        let nc = UINavigationController(rootViewController: vc)
        
        present(nc, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
        addRightButton()
    }
    
    func addRightButton() {
        let rightButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonClicked(sender:)))
        navigationItem.rightBarButtonItem = rightButton
    }
    
    func addLeftButton() {
        let leftButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneButtonClicked(sender:)))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "editExercisesCell", for: indexPath)
        cell.textLabel?.text = workout.exercises[indexPath.row].name
        cell.separatorInset = .zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workout.exercises.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
