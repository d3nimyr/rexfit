//
//  SelectExerciseViewController.swift
//  RexFit
//
//  Created by Hadevs on 20.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class SelectExerciseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView!
    
    var selectedCategory: String!
    private var exercises: [Exercise] = [] {
        didSet {
            tableView.reloadData()
            exercises = exercises.sorted(by: { (ex1, ex2) -> Bool in
                return ex1.name < ex2.name
            })
        }
    }
    
    var completion : ((Exercise) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        title = selectedCategory
        addRightButton()
    }

    func addRightButton() {
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addExerciseButtonClicked(sender:)))
        navigationItem.rightBarButtonItem = button
    }
    
    func addExerciseButtonClicked(sender: UIBarButtonItem) {

        let storybard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = storybard.instantiateViewController(withIdentifier: "addExerciseViewControllerID") as! AddExerciseViewController
        vc.completion = {
            exercise in

            self.completion!(exercise)
        }
        
        present(vc.nc(), animated: true, completion: nil)
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        exercises = moreExerciseData[selectedCategory]!
    }
    
    func initTableView() {
        tableView = UITableView(frame: view.bounds)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SelectExerciseTableViewCell.self, forCellReuseIdentifier: "exerciseCell")
        
        view.addSubview(tableView)
    }
    
    func exerciseButtonClicked(sender: UIButton) {
        let exercise = exercises[sender.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "editExerciseViewControllerID") as! EditExerciseViewController
        vc.selectedExercise = exercise
        vc.oldCategory = exercise.category
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        completion!(exercises[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell", for: indexPath) as! SelectExerciseTableViewCell
    
        cell.label.text = exercises[indexPath.row].name
        cell.infoButton.addTarget(self, action: #selector(self.exerciseButtonClicked(sender:)), for: .touchUpInside)
        cell.accessoryType = .disclosureIndicator
        cell.infoButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exercises.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    func nc() -> UINavigationController {
        return UINavigationController.init(rootViewController: self)
    }
}
