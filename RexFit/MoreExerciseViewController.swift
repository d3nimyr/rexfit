//
//  MoreExerciseViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

public var mappedValues: [Exercise] = [] {
    didSet {
        mappedValues = mappedValues.sorted(by: { (s1, s2) -> Bool in
            return s1.name < s2.name
        })
    }
}

class MoreExerciseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var moreExercisesViewControllerTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var filteredData = [String]() {
        didSet {
            filteredData = filteredData.sorted(by: { (s1, s2) -> Bool in
                return s1 < s2
            })
            moreExercisesViewControllerTable.reloadData()
        }
    }
    
    var exerciseNames = [String]() {
        didSet {
            exerciseNames = exerciseNames.sorted(by: { (s1, s2) -> Bool in
                return s1 < s2
            })
            
            moreExercisesViewControllerTable.reloadData()
        }
    }
    
    var isSearching = false
    
    var mappedDefaultExerciese: [DefaultExercise] =  []
    func addTapped(sender: UIBarButtonItem){
        presentWithNC(withIdentifier: "addExerciseViewControllerID")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moreExercisesViewControllerTable.dataSource = self
        moreExercisesViewControllerTable.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        title = NSLocalizedString("exercises", comment: "")
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x3478F6)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addRightButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mappedValues = moreExerciseData.map { $0.value }.flatMap{$0} // Объединяем массивы в один
        for exercise in mappedValues {
            exerciseNames.append(exercise.name)
        }
        
        exerciseNames = exerciseNames.sorted(by: { (a1, a2) -> Bool in
            return a1 < a2
        })

        self.moreExercisesViewControllerTable.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        finishEditing()
    }
    
    func addRightButton() {
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped(sender:)))
        navigationItem.rightBarButtonItem = add
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreExercisesViewControllerCell")
        
        if isSearching {
            cell?.textLabel?.text = filteredData[indexPath.row]
            return cell!
        } else {
            cell?.textLabel?.text = mappedValues[indexPath.row].name
        }
        return cell!
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        } else {
            return mappedValues.count
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editExerciseViewControllerID") as! EditExerciseViewController
        
        if isSearching{
            for exercise in mappedValues where exercise.name == filteredData[indexPath.row] {
                vc.selectedExercise = exercise
                vc.oldCategory = exercise.category
                
                if exercise is DefaultExercise {
                    showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("cantEdit", comment: ""))
                    return
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            vc.selectedExercise = mappedValues[indexPath.row]
            vc.oldCategory = mappedValues[indexPath.row].category
            if mappedValues[indexPath.row] is DefaultExercise {
                showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("cantEdit", comment: ""))
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
     }
    
    func finishEditing() {
        searchBar.text = nil
        isSearching = false
        view.endEditing(true)
        moreExercisesViewControllerTable.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        finishEditing()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text!.isEmpty {
            finishEditing()
        } else {
            isSearching = true
            filteredData = Array(Set(searchText.isEmpty ? exerciseNames : exerciseNames.filter({(dataString: String) -> Bool in
                return dataString.lowercased().contains(searchText.lowercased())
            })))
            moreExercisesViewControllerTable.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
