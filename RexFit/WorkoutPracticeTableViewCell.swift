//
//  PracticeTableViewCell.swift
//  RexFit
//
//  Created by Quaka on 20.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class WorkoutPracticeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var practiceLabel: UILabel!
    @IBOutlet weak var practiceTextField: UITextField!
    @IBOutlet weak var clickedButtonPractice: UIButton!

    var exercise: WorkoutExercise! {
        didSet {
            practiceLabel.text = exercise.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
