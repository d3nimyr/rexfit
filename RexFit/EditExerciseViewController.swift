//
//  EditExerciseViewController.swift
//  RexFit
//
//  Created by Никита on 14.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class EditExerciseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var editExerciseTable: UITableView!
    
    var selectedExercise: Exercise!
    var oldCategory: String!
    
    fileprivate let types = ["Strength", "Cardio", "Other"]
    fileprivate var selectedType = String()
    
    var pickerStatus = 0 {
        didSet {
            let indexPath = IndexPath(row: 3, section: 0)
            if pickerStatus == 1 {
                editExerciseTable.insertRows(at: [indexPath], with: .top)
            } else {
                editExerciseTable.deleteRows(at: [indexPath], with: .top)
            }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedType = types[row]
        editExerciseTable.reloadData()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return types[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func back(sender: UIBarButtonItem) {
        if !(selectedExercise is DefaultExercise) {
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = editExerciseTable.cellForRow(at: indexPath) as! EditExerciseTableViewCell
            guard let text = cell.textField.text, !text.isEmpty else {
                showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("nameFieldMustFilled", comment: ""))
                return
            }
        
            let indexPath1 = IndexPath(row: 1, section: 0)
            let cell1 = editExerciseTable.cellForRow(at: indexPath1)
            guard let category = cell1?.detailTextLabel?.text, !category.isEmpty else {
                showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("categoryFieldMustFilled", comment: ""))
                return
            }
            let newExercise = Exercise(name: text, category: category, type: .strength)
            let index = moreExerciseData[oldCategory]?.index(of: selectedExercise)
            
            moreExerciseData[oldCategory]!.remove(at: index!)
            moreExerciseData[selectedExercise.category]?.append(newExercise)
            
            let exerciseDict = ["name": text, "category": category, "type": selectedType.lowercased()] // change
            let oldExerciseDict = ["name": selectedExercise.name, "category": oldCategory!, "type": String(describing: selectedExercise.type).lowercased()]
            var indexDefault = 0
            var checker = false
            for element in defaultExerciseArray {
                indexDefault = indexDefault + 1
                if element == oldExerciseDict {
                    checker = true
                    indexDefault = indexDefault - 1
                    break
                }
            }
            
            if checker {
                defaultExerciseArray.remove(at: indexDefault)
            }
            defaultExerciseArray.append(exerciseDict as [String : String])
            defaults.set(defaultExerciseArray, forKey: "defaultExerciseArray")
            defaults.synchronize()
        }
        
        smartBack()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedType = String(describing: selectedExercise.type)
        editExerciseTable.dataSource = self
        editExerciseTable.delegate = self
        
        editExerciseTable.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
        oldCategory = selectedExercise.category
        print("wowe: \(oldCategory)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addLeftButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.editExerciseTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseNameCell") as? EditExerciseTableViewCell
            cell?.textField?.text = selectedExercise.name
            cell?.textField.autocapitalizationType = .words
            cell?.separatorInset = .zero
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseTypeCell")
            cell?.detailTextLabel?.text = selectedExercise.category
            cell?.textLabel?.text = NSLocalizedString("category", comment: "")
            cell?.separatorInset = .zero
            return cell!
        case 2:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseCategoryCell")
//            cell?.detailTextLabel?.text = selectedType.capitalized
//            cell?.separatorInset = .zero
//            cell?.textLabel?.text = NSLocalizedString("type", comment: "")
//            return cell!
            let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseSpaceCell")
            cell?.selectionStyle = .none
            cell?.separatorInset = .zero
            return cell!

        case 3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "deleteExerciseCell")
            cell?.separatorInset = .zero
            return cell!
//            if pickerStatus == 1 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "typeEditorcellId") as? TypeEditorTableViewCell
//                cell?.typeEditor.dataSource = self
//                cell?.separatorInset = UIEdgeInsets.zero
//                cell?.typeEditor.delegate = self
//                return cell!
//            } else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseSpaceCell")
//                cell?.selectionStyle = .none
//                cell?.separatorInset = .zero
//                return cell!
//            }
        case 4:
            if pickerStatus == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "editExerciseSpaceCell")
                cell?.separatorInset = .zero
                cell?.selectionStyle = .none
                return cell!
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "deleteExerciseCell")
                cell?.separatorInset = .zero
                return cell!
            }
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "deleteExerciseCell")
            cell?.separatorInset = .zero
            return cell!
        default: break
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 && pickerStatus == 1 {
            return 117
        } else {
            return 44
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 3 {
//            return 117
//        } else {
//            return 44
//        }
//    }
    func showAlertControllerWith(completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: NSLocalizedString("confirm", comment: ""), message: NSLocalizedString("areYouSure", comment: ""), preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .destructive) { (_) in
            completion()
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 5 + pickerStatus
        return 4
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "chooseCategoryViewControllerID") as! ChooseCategoryViewController
            vc.selectSender = false
            
            
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 3 {
            if !(self.selectedExercise is DefaultExercise) {
                showAlertControllerWith {
                    let exerciseDict = ["name": self.selectedExercise.name,"category": self.selectedExercise.category, "type": String(describing: self.selectedExercise.type)]
                    
                    guard let indexID = moreExerciseData[self.selectedExercise.category]?.index(of: self.selectedExercise) else {
                        return
                    }
                    
                    var indexDefault = 0
                    var checker = false
                    for element in defaultExerciseArray {
                        indexDefault = indexDefault + 1
                        if element == exerciseDict {
                            checker = true
                            indexDefault = indexDefault - 1
                            break
                        }
                    }
                    
                    moreExerciseData[self.selectedExercise.category]?.remove(at: indexID)
                    if checker {
                        defaultExerciseArray.remove(at: indexDefault)
                    }
                    defaults.set(defaultExerciseArray, forKey: "defaultExerciseArray")
                    defaults.synchronize()
                    
//                    Standart exercise couldnt be deleted.
                    
                    self.smartBack()
                }
            } else {
                showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("standartExerciseCouldntDeleted", comment: ""))
            }
        }
    }
    
    func addLeftButton(){
        let buttonBack = UIBarButtonItem(title: NSLocalizedString("back", comment: ""), style: .plain, target: self, action:  #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = buttonBack
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
