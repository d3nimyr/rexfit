//
//  HistoryTableViewCell.swift
//  RexFit
//
//  Created by Quaka on 01.08.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var dateHistoryLabel: UILabel!
    @IBOutlet weak var setsHistoryLabel: UILabel!
    @IBOutlet weak var charasteristicHistoryLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
