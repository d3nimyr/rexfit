
//
//  WorkoutTableViewCell.swift
//  RexFit
//
//  Created by Hadevs on 19.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class WorkoutsTableViewCell: UITableViewCell {

    @IBOutlet weak var dateDayLabel: UILabel!
    @IBOutlet weak var dateNumberLabel: UILabel!
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var exerciseNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(workout: Workout) {
        
        dateDayLabel.text = workout.startDate.convertFormateToNormDateString(format: "E")
        dateNumberLabel.text = workout.startDate.convertFormateToNormDateString(format: "d")
        workoutNameLabel.text = workout.name
        exerciseNameLabel.text = workout.exercises.map({ (exercise) -> String in
            return exercise.name
        }).joined(separator: ", ")
        
        if let endDate = workout.endDate {
            let seconds = endDate.seconds(from: workout.startDate)
            let minutes = endDate.minutes(from: workout.startDate)
//            let hours = endDate.hours(from: workout.startDate)
//            let days = endDate.days(from: workout.startDate)
//            let weeks = endDate.weeks(from: workout.startDate)
//            let mounths = endDate.months(from: workout.startDate)
            
            if seconds < 60 {
                timeLabel.text = "\(seconds) \("secs".localized)"
            } else {
                timeLabel.text = "\(minutes) \("mins".localized)"
            }
            
    //        } else if hours <= 24 {
    //            timeLabel.text = "\(hours) hours"
    //        } else if days <= 7 {
    //            timeLabel.text = "\(days) days"
    //        } else if weeks <= 4 {
    //            timeLabel.text = "\(weeks) weeks"
    //        } else if mounths < 12 {
    //            timeLabel.text = "\(mounths) months"
    //        }
        
            if seconds == 0 {
                timeLabel.text = nil
            }
        } else {
            timeLabel.text = nil
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    
    }
    
}
    

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}


extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
