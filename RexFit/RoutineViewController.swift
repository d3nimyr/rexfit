//
//  RoutineViewController.swift
//  RexFit
//
//  Created by Hadevs on 25/09/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import StoreKit

class RoutineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKProductsRequestDelegate, UITextFieldDelegate {
    
    var routine: RoutineWorkout = {
        return RoutineWorkout.empty()
    }()
    
    var isExercisesExist: Bool {
        return !routine.exercises.isEmpty
    }
    
    var GRAY_COLOR: UIColor = UIColor(netHex: 0xf1f1f1)
    var BLUE_COLOR: UIColor = UIColor(netHex: 0x3478F6)
    
    private var donePanel: DonePanel!
    var activeTextField: UITextField = UITextField()
    var defaultOffset: CGPoint = .zero
    
    var canEdit: Bool!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
        tableView.keyboardDismissMode = .onDrag
        delegating()
        tableView.reloadData()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChangeFrame(_:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
        donePanel = DonePanel(frame: CGRect.zero)
        donePanel.isNeedToHideArrows = true
        view.addSubview(donePanel)
        appearDonePanel(withSelector: #selector(rightButtonClicked(sender:)))
//        addRightBarButton()
        setRightBarButton(with: .edit)
    }
    
//    func addRightBarButton() {
//        let rightBarButton = UIBarButtonItem.init(title: L.s("edit"), style: .Plain, target: self, action: #selector(rightBarButtonClicked(sender:)))
//        navigationItem.rightBarButtonItem = rightBarButton
//    }
//
//    @objc private func rightBarButtonClicked(sender: UIBarButtonItem) {
//
//    }
//
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delay(delay: 0.25) {
            let pointInTable: CGPoint = textField.superview!.convert(textField.frame.origin, to: self.tableView)
            self.activeTextField = textField
            var contentOffset = self.tableView.contentOffset
            contentOffset.y = pointInTable.y - 220
            self.tableView.setContentOffset(contentOffset, animated: true)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if activeTextField == UITextField() {
            tableView.setContentOffset(defaultOffset, animated: true)
        } else {
            activeTextField = UITextField()
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setLeftBarButton(with: .done)
    }
    
    func setLeftBarButton(with item: UIBarButtonSystemItem) {
        let button = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(leftBarButtonClicked(sender:)))
        navigationItem.leftBarButtonItem = button
    }
    
    func setRightBarButton(with item: UIBarButtonSystemItem) {
        let button = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(rightBarButtonClicked(sender:)))
        navigationItem.rightBarButtonItem = button
    }
    
    func leftBarButtonClicked(sender: UIBarButtonItem) {
        routine.name = (routine.name ?? "").isEmpty ? NSLocalizedString("untitledWorkout", comment: "") : routine.name!
        
        if routine.isCreated == false && canEdit == true {
            routine.isCreated = true
            routine.id = ID()
            if routines.count >= 1 && !premiumOrdered {
                self.requestBuyPremium()
            } else {
                routines.insert(routine, at: 0)
            }
        } else {
            for mainRoutine in routines {
                let index = routines.index(of: mainRoutine)!
                if mainRoutine.id == routine.id {
                    routines[index] = routine
                    break
                }
            }
        }
        
        routine = RoutineWorkout.empty()
        
        smartBack()
    }
    
    func rightBarButtonClicked(sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        if tableView.isEditing {
            setRightBarButton(with: .done)
        } else {
            setRightBarButton(with: .edit)
        }
        
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let x = routine.exercises[sourceIndexPath.row - (5 + (isExercisesExist ? 1 : 0))]
        
        routine.exercises.remove(at: sourceIndexPath.row - (5 + (isExercisesExist ? 1 : 0)))
        routine.exercises.insert(x, at: destinationIndexPath.row - (5 + (isExercisesExist ? 1 : 0)))
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            routine.exercises.remove(at: indexPath.row - (5 + (isExercisesExist ? 1 : 0)))
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if (proposedDestinationIndexPath.row >= 6 && proposedDestinationIndexPath.row <= 5 + routine.exercises.count) == false {
            return sourceIndexPath
        }
        
        return proposedDestinationIndexPath
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row > 4 + (isExercisesExist ? 1 : 0) && indexPath.row <= 4 + (isExercisesExist ? 1 : 0) + routine.exercises.count && canEdit
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    func delegating() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        view.endEditing(true)
        
        switch indexPath.row {
            case 1:
                //clicked at "start this workout"
                startThisWorkout()
            case 4:
                navigationController?.pushViewController(TextViewViewController(), animated: true)
            case 6 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                let vc = SelectCategoryTableViewController()
                let comp: ((Exercise) -> Void) = {
                    exercise in
                    
                    self.routine.exercises.append(WorkoutExercise(exercise: exercise))
                    tableView.reloadData()
                }
                
                vc.completion = comp
                let nc = UINavigationController(rootViewController: vc)
                present(nc, animated: true, completion: nil)
            
            case 4 + (isExercisesExist ? 1 : 0)...4 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                if let cell = tableView.cellForRow(at: indexPath) as? BasicTableViewCell {
                    cell.detailTextField.becomeFirstResponder()
                }
            default: break
        }
    }
    
    func startThisWorkout() {
        let workout = Workout(routine: routine)
        smartBack {
            if let window = UIApplication.shared.keyWindow, let tabBar = window.rootViewController as? UITabBarController{
                tabBar.selectedIndex = 0
                
                // no we neeed to open WorkoutViewController
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "workoutVC") as? WorkoutViewController else {
                    return
                }
                
                vc.workout = workout
                
                tabBar.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
        }
    }
    
    func appearDonePanel(withSelector selector: Selector) {
        let frame = CGRect(x: 0, y: view.frame.maxY, width: view.frame.width, height: 44)
        donePanel.frame = frame
        donePanel.doneButton.removeTarget(nil, action: nil, for: .allEvents)
        donePanel.doneButton.addTarget(self, action: selector, for: .touchUpInside)
    }
    
    
    func hideDonePanel() {
        UIView.animate(withDuration: 0.15, animations: {
            self.donePanel.frame.origin.y = 1500
        })
    }
    
    func rightButtonClicked(sender: UIButton) {
        view.endEditing(true)
        hideDonePanel()
    }
    
    func keyboardWillHide(_ notification: Notification) {

        tableView.contentOffset.y = 0
        hideDonePanel()
    }
    
    func keyboardChangeFrame(_ notification:Notification) {
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        donePanel.frame.origin.y = keyboardRect.origin.y - donePanel.frame.height
    }
    
    func keyboardWillShow(_ notification:Notification) {
        
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        tableView.contentOffset.y += keyboardRect.height / 2
//        donePanel.frame.origin.y = keyboardRect.origin.y - donePanel.frame.height
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let exercise = routine.exercises[indexPath.row - (5 + (isExercisesExist ? 1 : 0))]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "singleExerciseViewControllerID") as! SingleExerciseViewController
        
        guard let defaultExercise = exercise.toDefaultExercise() else {
            self.showAlert(title: NSLocalizedString("error", comment: ""), message: "Selected exercise is not default. Please, select another one.")
            return
        }
        
        if moreExerciseData[exercise.category]!.sorted(by: { (ex1, ex2) -> Bool in
            return ex2.name > ex1.name
        }).index(of: exercise)! > 2 && !premiumOrdered {
            let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("premiumAlert", comment: ""), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Buy", style: .default, handler: { (_) in
                self.requestBuyPremium()
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alertController.addAction(yesAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        vc.selectedexercise = defaultExercise
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func requestBuyPremium() {
        if (SKPaymentQueue.canMakePayments()) {
            
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: NSSet(object: productID) as! Set<String>)
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct) {
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    showAlert(title: "Success", message: "Premium has been purchased!")
                    set(success: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .failed:
                    print("Purchased Failed");
                    showAlert(title: "Error", message: "Error with ordering premium. Please, try again.")
                    set(success: false)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .restored:
                    print("Already Purchased");
                    showAlert(title: "Error", message: "Premium has been already purchased.")
                    set(success: false)
                    SKPaymentQueue.default().restoreCompletedTransactions()
                default:
                    break;
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.row {
            
        case 4 + (isExercisesExist ? 1 : 0)...4 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
            print("wow: \(123)")
            return true
        default: return false
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let count : Int = response.products.count
        if (count > 0) {
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == productID) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(product: validProduct);
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            print("nothing")
        }
    }
    
    
    func set(success: Bool) {
        
        if defaults.bool(forKey: "premium") == false {
            defaults.set(success, forKey: "premium")
            defaults.synchronize()
        }
        
        premiumOrdered = success
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            case 0:
                // empty cell
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
                cell.selectionStyle = .none
                cell.separatorInset = .zero
                return cell
            case 1:
                let cell = UITableViewCell()
                cell.textLabel?.textColor  = BLUE_COLOR
                cell.textLabel?.text       = NSLocalizedString("startThisWorkout", comment: "")
                cell.separatorInset = .zero
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailTableViewCell
                cell.detailLabel.text      = NSLocalizedString("details", comment: "").uppercased()
                cell.rightLabel.isHidden   = true
                cell.selectionStyle = .none
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell", for: indexPath) as! TextFieldTableViewCell
                cell.textField.placeholder = NSLocalizedString("workoutName", comment: "")
                cell.textField.autocorrectionType = .no
                cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
                cell.textField.text = routine.name
                cell.textField.isUserInteractionEnabled = true
                
                cell.textChanged = {
                    text in
                    
                    self.routine.name = text
                }
                
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell", for: indexPath) as! TextFieldTableViewCell
                cell.textField.placeholder = NSLocalizedString("notes", comment: "")
                cell.textField.isUserInteractionEnabled = false
                cell.textField.text = routine.notes
                
                cell.accessoryType = .disclosureIndicator
                cell.separatorInset = .zero
                return cell
            case 4 + (isExercisesExist ? 1 : 0):
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailTableViewCell
                cell.detailLabel.text     = NSLocalizedString("exercises", comment: "").uppercased()
                cell.rightLabel.isHidden  = false
                cell.selectionStyle = .none
                return cell
            case 4 + (isExercisesExist ? 1 : 0)...4 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                let cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath) as! BasicTableViewCell
                cell.separatorInset = .zero
                let exercise = routine.exercises[indexPath.row - (5 + (isExercisesExist ? 1 : 0))]
                cell.textField.text = exercise.name
                cell.detailTextField.delegate = self
                cell.detailTextField.text = "\(exercise.sets.count)"
                cell.detailTextField.autocorrectionType = .no
                cell.setsCountChanged = {
                    text in
                    
                    if let setsCount = Int(text) {
                        exercise.sets.removeAll()
                        for _ in 0..<setsCount {
                            exercise.addSet(set: ExerciseSet(weight: 0, reps: 0))
                        }
                    }
                }
                cell.accessoryType  = .detailButton
                
                return cell
            case 5 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = GRAY_COLOR
                cell.separatorInset = .zero
                return cell
            case 6 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                let cell = UITableViewCell()
                cell.textLabel?.textColor = BLUE_COLOR
                cell.textLabel?.text      = NSLocalizedString("addExercise", comment: "")
                cell.separatorInset = .zero
                return cell
            default:
                return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7 + (isExercisesExist ? 1 : 0) + routine.exercises.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
            case 0, 1, 3, 4, 4 + (isExercisesExist ? 1 : 0)...4 + (isExercisesExist ? 1 : 0) + routine.exercises.count, 6 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                return 44
            case 2:
                return 54
            case 5 + (isExercisesExist ? 1 : 0) + routine.exercises.count:
                return 20
            default:
                return 44
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
