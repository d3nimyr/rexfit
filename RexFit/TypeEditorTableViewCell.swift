//
//  TypeEditorTableViewCell.swift
//  RexFit
//
//  Created by Никита on 17.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class TypeEditorTableViewCell: UITableViewCell {
    @IBOutlet weak var typeEditor: UIPickerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
