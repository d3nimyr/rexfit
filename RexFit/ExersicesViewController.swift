//
//  ExersicesViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import StoreKit

class ExersicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver  {
    
    @IBOutlet weak var exetciseTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var filteredData = [String]() {
        didSet {
            filteredData = filteredData.sorted(by: { (s1, s2) -> Bool in
                return s1 < s2
            })
        }
    }
    
    var isSearching = false
    var selectedCategory: String!
    var exersices = [DefaultExercise]()
    var mappedValues = [DefaultExercise]()
    
    var exerciseNames = [String]() {
        didSet {
            exerciseNames = exerciseNames.sorted(by: { (s1, s2) -> Bool in
                return s1 < s2
            })
        }
    }
    
    var exerciseNamesSearch = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exetciseTable.dataSource = self
        exetciseTable.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        mappedValues = exerciseData.map { $0.value }.flatMap{$0}
        exersices = exerciseData[selectedCategory]!
        exerciseNames = exersices.map({ (exercise) -> String in
            return exercise.name
        })
        exerciseNamesSearch = mappedValues.map({ (exercise) -> String in
            return exercise.name
        })
        
        title = NSLocalizedString("exercises", comment: "") // можно просто тайтл
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x3478F6)
        
        SKPaymentQueue.default().add(self)
    }
    
    func requestBuyPremium() {
        if (SKPaymentQueue.canMakePayments()) {
            
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: NSSet(object: productID) as! Set<String>)
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct) {
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    showAlert(title: "Success", message: "Premium has been purchased!")
                    set(success: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .failed:
                    print("Purchased Failed");
                    showAlert(title: "Error", message: "Error with ordering premium. Please, try again.")
                    set(success: false)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .restored:
                    print("Already Purchased");
                    showAlert(title: "Error", message: "Premium has been already purchased.")
                    set(success: false)
                    SKPaymentQueue.default().restoreCompletedTransactions()
                default:
                    break;
                }
            }
        }
    }
    
    func set(success: Bool) {
        
        if defaults.bool(forKey: "premium") == false {
            defaults.set(success, forKey: "premium")
            defaults.synchronize()
        }
        
        premiumOrdered = success
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let count : Int = response.products.count
        if (count > 0) {
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == productID) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(product: validProduct);
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            print("nothing")
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell")
            cell?.textLabel?.text = filteredData[indexPath.row]
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell")
            cell?.textLabel?.text = exerciseNames[indexPath.row]
            return cell!
        }
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        } else {
            return exerciseNames.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if premiumOrdered || indexPath.row < 3 {
     
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "singleExerciseViewControllerID") as! SingleExerciseViewController
            
            var values: [String] {
                if isSearching {
                    return filteredData.sorted()
                } else {
                    return exerciseNames.sorted()
                }
            }
        
            
            if isSearching{
                for exercise in mappedValues where exercise.name == values[indexPath.row] && exercise.description == values[indexPath.row].description {
                        vc.selectedexercise = exercise
                }
            } else {
                vc.selectedexercise = exersices.filter({ (ex) -> Bool in
                    return ex.name == values[indexPath.row]
                })[0]
            }
            
            if vc.selectedexercise != nil {
                navigationController?.pushViewController(vc, animated: true)
           }
        } else {
            let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("premiumAlert", comment: ""), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Buy", style: .default, handler: { (_) in
                self.requestBuyPremium()
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alertController.addAction(yesAction)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func finishEditing() {
        searchBar.text = nil
        isSearching = false
        view.endEditing(true)
        exetciseTable.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        finishEditing()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            finishEditing()
        } else {
            isSearching = true
            filteredData = Array(Set(searchText.isEmpty ? exerciseNamesSearch : exerciseNamesSearch.filter({(dataString: String) -> Bool in
                return dataString.lowercased().contains(searchText.lowercased())
            })))
            exetciseTable.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
