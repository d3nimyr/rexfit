//
//  Routine.swift
//  RexFit
//
//  Created by Hadevs on 26/09/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

public class RoutineWorkout: NSObject {
    var name:  String? {
        didSet {
            if (name ?? "").isEmpty {
                name = NSLocalizedString("untitledWorkout", comment: "")
            }
        }
    }
    var notes: String = ""
    var exercises: [WorkoutExercise] = []
    var id: String!
    
    var isCreated: Bool = false
    
    static func empty() -> RoutineWorkout {
        let workout = RoutineWorkout(name: "")
        
        workout.isCreated = false
        workout.exercises = []
        
        let time = String(Int(NSDate().timeIntervalSince1970), radix: 16, uppercase: false)
        let machine = String(arc4random_uniform(900000) + 100000)
        let pid = String(arc4random_uniform(9000) + 1000)
        let counter = String(arc4random_uniform(900000) + 100000)
        
        workout.id = time + machine + pid + counter
        
        return workout
    }
    
    init(name: String?) {
        self.name = name
    }
    
    init(name: String, defaultExercises: [DefaultExercise]) {
        super.init()
        self.name = name
        self.exercises = defaultExercises.map{ WorkoutExercise.init(exercise: $0) }
        self.id = self.ID()
    }
    
    init(from workout: Workout) {
        self.name = (workout.name ?? "").isEmpty ? NSLocalizedString("untitledWorkout", comment: "") : workout.name
        self.notes = workout.notes
        self.id = workout.id
        self.isCreated = true
        self.exercises = workout.exercises.map({ (exercise) -> WorkoutExercise in
            let returnExercise = exercise
            returnExercise.sets = returnExercise.sets.map({ (set) -> ExerciseSet in
                return ExerciseSet(weight: 0, reps: 0)
            })
            return returnExercise
        })
    }
    
    var dict: [String: Any] {
        return [
            "name": name ?? "",
            "notes": notes,
            "id": id,
            "isCreated": isCreated,
            "exercises": exercises.map({ (exercise) -> [String: Any] in
                return exercise.toDictionary()
            })
        ]
    }
    
    init(dict: [String: Any]) {
        self.name = (dict["name"] as! String)
        self.notes = dict["notes"] as! String
        self.isCreated = dict["isCreated"] as! Bool
        self.exercises = (dict["exercises"] as! [[String: Any]]).map({ (subDict) -> WorkoutExercise in
            return subDict.toWorkoutExercise()
        })
        self.id = dict["id"] as! String
    }
}
