//
//  SelectExerciseTableViewController.swift
//  RexFit
//
//  Created by Hadevs on 20.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

public var allExercises: [Exercise] = {
    return moreExerciseData.flatMap{$0.value}
}()

class SelectCategoryTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    private var categories: [String] = {
        return Array(moreExerciseData.keys).sorted()
        }()
        {
        didSet {
            tableView.reloadData()
        }
    }
    
    var completion : ((Exercise) -> Void)?
    
    private var searchedExercises: [Exercise] = [] {
        didSet {
            if searchedExercises.count > 0 {
                categories = searchedExercises.map({ (exercise) -> String in
                    return exercise.name
                }).sorted()
            } else {
                categories = Array(moreExerciseData.keys).sorted()
            }
        }
    }
    
    private var tableView: UITableView!
    private var searchBar: UISearchBar!
    
    func initSearchBar() {
        let rect = CGRect(x: 0, y: 62, width: view.frame.width, height: 44)
        searchBar = UISearchBar(frame: rect)
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("searchExercise", comment: "")
        
        view.addSubview(searchBar)
    }
    
    func initTableView() {
        tableView = UITableView(frame: view.bounds)
        view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame.origin.y = searchBar.frame.maxY
        tableView.frame.size.height -= searchBar.frame.height
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "textCell")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        categories = Array(moreExerciseData.keys)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            searchedExercises = []
        } else {
            searchedExercises = allExercises.filter({ (exercise) -> Bool in
                return exercise.name.lowercased().contains(searchText.lowercased())
            }).sorted(by: { (ex1, ex2) -> Bool in
                return ex1.name < ex2.name
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        title = NSLocalizedString("selectExercise", comment: "")


        initSearchBar()
        initTableView()
        
        view.backgroundColor = .white
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addRightButton()
        addLeftButton()
    }
    
    func addRightButton() {
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addExerciseButtonClicked(sender:)))
        navigationItem.rightBarButtonItem = button
    }
    
    func addLeftButton() {
        let button = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelButtonClicked(sender:)))
        navigationItem.leftBarButtonItem = button
    }
    
    func cancelButtonClicked(sender: UIBarButtonItem) {
        smartBack()
    }
    
    func addExerciseButtonClicked(sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let vc = storyboard.instantiateViewController(withIdentifier: "addExerciseViewControllerID") as? AddExerciseViewController else {
            return
        }
        
        vc.completion = {
            exercise in
            
            self.completion!(exercise)
            self.delay(delay: 1, closure: {
                print("heh!")
                self.smartBack()
            })
        }
        
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        categories = Array(moreExerciseData.keys).sorted()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if searchedExercises.count > 0 {
            completion!(searchedExercises[indexPath.row])
            smartBack()
        } else {
            let vc = SelectExerciseViewController()
            let comp: ((Exercise) -> Void) = {
                exercise in
                self.completion!(exercise)
                
                self.delay(delay: 0.5, closure: {
                    self.smartBack()
                })
            }
            vc.selectedCategory = categories[indexPath.row]
            vc.completion = comp
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categories.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "textCell", for: indexPath)
        cell.textLabel?.text = categories[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
