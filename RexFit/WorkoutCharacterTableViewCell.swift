//
//  CharacterTableViewCell.swift
//  RexFit
//
//  Created by Quaka on 20.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class WorkoutCharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var weightField: UITextField!
    @IBOutlet weak var repsField: UITextField!
    @IBOutlet weak var clickedButtonCharacter: UIButton!
    
    var workout: Workout!
    
    var set: ExerciseSet! {
        didSet {
            weightField.text = set.weight > 0 ? "\(set.weight)" : nil
            repsField.text = set.reps > 0 ? "\(set.reps)" : nil
            
            if workout.isRepeated {
                weightField.text = nil
                repsField.text = nil
                
                repsField.placeholder = set.reps > 0 ? "\(set.reps)" : nil
                weightField.placeholder = set.weight > 0 ? "\(set.weight)" : nil
            }
        }
    }
    
    var exercise: WorkoutExercise! {
        didSet {
            guard let lastExercise = getLastExercise(fromExercise: exercise), let indexOfSet: Int = exercise.sets.index(of: set), lastExercise.sets.contains(index: indexOfSet) else {
                weightField.placeholder = "0"
                repsField.placeholder = "0"
                return
            }
           
            let newSet = lastExercise.sets[indexOfSet]
            
            weightField.placeholder = "\(newSet.weight)"
            repsField.placeholder = "\(newSet.reps)"
//            weightField.text = workout.isRepeated || newSet.weight == 0 ? nil : "\(newSet.weight)"
//            repsField.text = workout.isRepeated || newSet.reps == 0 ? nil : "\(newSet.reps)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
