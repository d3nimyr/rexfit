//
//  TextFieldViewController.swift
//  RexFit
//
//  Created by Hadevs on 19.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class TextViewViewController: UIViewController {

    private var textView: UITextView!
    
    private func initTextView() {
        textView = UITextView(frame: view.bounds)
        textView.font = UIFont.systemFont(ofSize: 16.0)
        
        var savedText : String {
            return prevVC() is WorkoutViewController ? (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 2] as! WorkoutViewController).notesText : (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 2] as! RoutineViewController).routine.notes
        }
        
        textView.text = savedText
        view.addSubview(textView)
        
        textView.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTextView()
        
        title = NSLocalizedString("notes", comment: "")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if prevVC(offset: 1) is WorkoutViewController {
            print(navigationController!.childViewControllers.count)
            (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 1] as! WorkoutViewController).notesText = textView.text ?? ""
        } else if prevVC(offset: 1) is RoutineViewController {
            (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 1] as! RoutineViewController).routine.notes = textView.text ?? ""
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

public extension UIViewController {
    func prevVC(offset: Int = 2) -> UIViewController {
        return navigationController!.childViewControllers[navigationController!.childViewControllers.count - offset]
    }
}
