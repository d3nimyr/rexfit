//
//  MoreSettingTableViewCell.swift
//  RexFit
//
//  Created by Hadevs on 17.08.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class MoreSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var settingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
