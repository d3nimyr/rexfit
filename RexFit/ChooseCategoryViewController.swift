//
//  ChooseCategoryViewController.swift
//  RexFit
//
//  Created by Никита on 12.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class ChooseCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func cancel(sender: UIBarButtonItem) {
        smartBack()
    }
    
    @IBOutlet weak var chooseCategoryTable: UITableView!
    
    var completion: ((String) -> Void)?
    var selectSender = true
    
    func addTapped(sender: UIBarButtonItem){
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "addCategoryViewControllerID") as? AddCategoryViewController else {
            return
        }
        
        vc.completion = {
            category in
            
            self.select(category: category)
            
            if self.completion != nil {
                self.completion!(category)
            }
            
            self.smartBack()
        }
        
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseCategoryTable.dataSource = self
        chooseCategoryTable.delegate = self
        addLeftButton()
        addRightButton()
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x3478F6)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        moreCategories = Array(moreExerciseData.keys).sorted()
        chooseCategoryTable.reloadData()
    }
    
    func addRightButton() {
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped(sender:)))
        navigationItem.rightBarButtonItem = add
    }
    
    func addLeftButton() {
        let buttonCancel = UIBarButtonItem(title: NSLocalizedString("cancel", comment: ""), style: .plain, target: self, action:  #selector(cancel(sender:)))
        self.navigationItem.leftBarButtonItem = buttonCancel
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chooseCategoryCellID")
        cell?.textLabel?.text = moreCategories[indexPath.row]
        return cell!
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moreCategories.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        select(category: moreCategories[indexPath.row])
    }
    
    func select(category: String) {
        if selectSender {
            chosenCategory = category
            dismiss(animated: true, completion: nil)
        } else {
            (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 2] as! EditExerciseViewController).selectedExercise.category = category
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
