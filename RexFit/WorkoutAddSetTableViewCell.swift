//
//  AddSetTableViewCell.swift
//  RexFit
//
//  Created by Quaka on 20.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class WorkoutAddSetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var clickedButtonAddSet: UIButton!
    @IBOutlet weak var addSetLabel: UILabel!
    @IBOutlet weak var historyButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
