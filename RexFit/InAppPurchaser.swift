//
//  InAppPurchaser.swift
//  RexFit
//
//  Created by Hadevs on 18.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import StoreKit

public let productID =
"sergeykushner.RexFit.Premium"

class InAppPurchaser: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.description)
        let count : Int = response.products.count
        if (count>0) {
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == productID) {
                buyProduct(validProduct)
            } else {
                print(validProduct.productIdentifier)
            }
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        set(success: false)
    }
    
    fileprivate var purchasedProductIdentifiers = Set<String>()
    fileprivate var productsRequest: SKProductsRequest?
    
    var completion: ((Bool) -> Void)!
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                SKPaymentQueue.default().finishTransaction(transaction)
                set(success: true)
            case SKPaymentTransactionState.failed:
                SKPaymentQueue.default().finishTransaction(transaction)
                set(success: false)
            default:
                print("default: \(transaction.transactionState.rawValue)")
            }
        }
    }
    
    func set(success: Bool) {
        completion(success)
        
        if defaults.bool(forKey: "premium") == false {
            defaults.set(success, forKey: "premium")
            defaults.synchronize()
        }
        
        premiumOrdered = success
    }
    
    public func requestProducts() {
        productsRequest?.cancel()
        let productIdentifiers: Set<String> = [productID]
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self
        productsRequest!.start()
    }
    
    override init() {
        super.init()
        SKPaymentQueue.default().add(self)
    }
    
    private func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
 
}
