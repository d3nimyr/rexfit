//
//  AppDelegate.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import Firebase
import StoreKit

public var premiumOrdered: Bool = false
public var moreExerciseData: [String : [Exercise]] = [:]
public var defaultCategoriesArray: [String] = (defaults.array(forKey: "defaultCategoriesArray") as? [String]) ?? []
public var defaultExerciseArray = defaults.array(forKey: "defaultExerciseArray") as? [[String: String]] ?? []
public var moreCategories = [String]()
public var routines: [RoutineWorkout] = [] {
    didSet {
        defaults.set(routines.map {$0.dict}, forKey: "routines")
        defaults.synchronize()
    }
}

public var lastExercises: [WorkoutExercise] = [] {
    didSet {
        let arrayOfDicts: [[String: Any]] = lastExercises.map { (exercise) -> [String: Any] in
            return exercise.toDictionary()
        }
        
        defaults.set(arrayOfDicts, forKey: "lastExercises")
        defaults.synchronize()
    }
}

public var savedWeight: Double! {
    didSet {
        defaults.set(savedWeight, forKey: "bodyWeight")
        defaults.synchronize()
    }
}

public func loadWorkouts() {
    guard let localWorkouts = defaults.value(forKey: "workouts") as? [NSDictionary] else {
        return
    }
    
    publicWorkouts = localWorkouts.toWorkoutsArray()
}

fileprivate let minimumHitArea = CGSize(width: 44, height: 44)

extension UIButton {
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // if the button is hidden/disabled/transparent it can't be hit
        if self.isHidden || !self.isUserInteractionEnabled || self.alpha < 0.01 { return nil }
        
        // increase the hit frame to be at least as big as `minimumHitArea`
        let buttonSize = self.bounds.size
        let widthToAdd = max(minimumHitArea.width - buttonSize.width, 0)
        let heightToAdd = max(minimumHitArea.height - buttonSize.height, 0)
        let largerFrame = self.bounds.insetBy(dx: -widthToAdd / 2, dy: -heightToAdd / 2)
        
        // perform hit test on larger frame
        return (largerFrame.contains(point)) ? self : nil
    }
}

public func loadExerciseData() {
    moreExerciseData = exerciseData
    for key in defaultCategoriesArray {
        if moreExerciseData[key] == nil {
            moreExerciseData.updateValue([], forKey: key)
        }
    }
    for exercise in defaultExerciseArray {
        moreExerciseData[exercise["category"]!]?.append(Exercise(name: exercise["name"]!, category: exercise["category"]!, type: exercise["type"]!.convertToEnum()))
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func loadSavedWeight() {
        savedWeight = defaults.value(forKey: "bodyWeight") as? Double
    }
    
    func loadLastExercises() {
        guard let exercisesDict = defaults.object(forKey: "lastExercises") as? [[String: Any]] else {
            return
        }
        
        lastExercises =  exercisesDict.map { (dict) -> WorkoutExercise in
            return dict.toWorkoutExercise()
        }
    }
    
    func loadSavedRoutines() {
        guard let dictOfRoutines = defaults.value(forKey: "routines") as? [[String: Any]] else {
            return
        }
        
        routines = dictOfRoutines.map({ (dict) -> RoutineWorkout in
            return RoutineWorkout(dict: dict)
        })
    }
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        loadExerciseData()
        loadWorkouts()
        loadLastExercises()
        loadSavedRoutines()
        loadSavedWeight()
        requestRate()
        premiumOrdered = defaults.bool(forKey: "premium")
        
        return true
    }
    
    func requestRate() {
        if arc4random_uniform(11) == 1 {
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

