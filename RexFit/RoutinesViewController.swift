//
//  RoutinesViewController.swift
//  RexFit
//
//  Created by Hadevs on 25/09/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class RoutinesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmenter: UISegmentedControl!
    
    var currentRoutines: [RoutineWorkout] = {
       return routines
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.titleView = nil // for disable segmenter and enable text
        
        delegating()
        addRightBarButton()
        
        title = NSLocalizedString("routines", comment: "")
        
        
//        segmenter.setTitle(NSLocalizedString("myRoutines", comment: ""), forSegmentAt: 0)
//        segmenter.setTitle(NSLocalizedString("theBest", comment: ""), forSegmentAt: 1)
        
        let leftButton = UIBarButtonItem(title: NSLocalizedString("edit", comment: ""), style: .plain, target: self, action: #selector(editButtonClicked(sender:)))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    func addRightBarButton() {
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonClicked))
        navigationItem.rightBarButtonItem = button
    }
    
    func addButtonClicked() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "routineVC") as? RoutineViewController else {
            return
        }
        func process()
        {
            vc.routine = RoutineWorkout.empty()
            //        vc.canEdit = segmenter.selectedSegmentIndex == 0
            vc.canEdit = true
            navigationController?.pushViewController(vc, animated: true)
        }
        
        if premiumOrdered || currentRoutines.count == 0 {
            process()
        } else {
            showAlert(title: NSLocalizedString("error", comment: ""), message: L.s("shouldBuyPremium"))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//
//        guard let segmenter = segmenter else {
//            return
//        }
//
//        switch segmenter.selectedSegmentIndex {
//            case 0:
//                currentRoutines = routines
//            default:
//                currentRoutines = deafultRoutines
//        }
        
        currentRoutines = routines
        tableView.reloadData()
    }
    
    @IBAction func segmentChanged(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 0:
                currentRoutines = routines
            default:
                currentRoutines = deafultRoutines
        }
        tableView.reloadData()
    }
    
    @objc private func editButtonClicked(sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: tableView.isEditing ? NSLocalizedString("complete", comment: "") : NSLocalizedString("edit", comment: ""), style: .plain, target: self, action: #selector(editButtonClicked(sender:)))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let routine = currentRoutines[indexPath.row]
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "routineVC") as? RoutineViewController {
            vc.routine = routine
//            vc.canEdit = segmenter.selectedSegmentIndex == 0
            vc.canEdit = true
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = array
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        
        return arr
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            currentRoutines.remove(at: indexPath.row)
            routines.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let lastRow = sourceIndexPath.row
        let newRow = destinationIndexPath.row
        
        currentRoutines = rearrange(array: currentRoutines, fromIndex: lastRow, toIndex: newRow)
        routines = rearrange(array: routines, fromIndex: lastRow, toIndex: newRow)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return segmenter?.selectedSegmentIndex ?? 0 == 0
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let routine = currentRoutines[indexPath.row]
        
        cell.textLabel?.text = routine.name
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentRoutines.count
    }
    
    func delegating() {
        tableView.delegate   = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class L {
    static func s (_ s: String) -> String {
        return NSLocalizedString(s, comment: "")
    }
}
