//
//  BasicTableViewCell.swift
//  RexFit
//
//  Created by Hadevs on 26/09/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class BasicTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var detailTextField: UITextField!
    
    var setsCountChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textField.isEnabled = false
        
        detailTextField.delegate = self
        detailTextField.keyboardType = .numberPad
        detailTextField.placeholder = "0"
        // Initialization code
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = textField.text?.removeText(text: " sets")
        textField.text = nil
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = "\((textField.text ?? "").removeText(text: " sets"))"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let updatedText = textFieldText.replacingCharacters(in: range, with: string)
        
        if let completion = setsCountChanged {
            completion(updatedText)
        }
        
        return true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
