//
//  ViewController.swift
//  RexFit
//
//  Created by Никита on 09.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

let defaults = UserDefaults.standard


public class Exercise {
    var name: String
    var category: String
    var type: ExerciseType
    
    init(name: String, category: String, type: ExerciseType) {
        self.name = name
        self.type = type
        self.category = category
    }
}

enum ExerciseType {
    case strength
    case cardio
    case other
}

extension Exercise: Equatable {
    public static func == (lhs: Exercise, rhs: Exercise) -> Bool {
        return
                lhs.name == rhs.name &&
                lhs.type == rhs.type &&
                lhs.category == rhs.category
    }
}

public class DefaultExercise: Exercise {
    var description: String
    var image: [UIImage]
    
    init(description: String, image: [UIImage], name: String, category: String, type: ExerciseType) {
        self.description = description
        self.image = image
        super.init(name: name, category: category, type: type)
    }
}

public var categories = [String](exerciseData.keys.sorted())

class CategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var filteredData = [String]()
    var isSearching = false
    var mappedValues = [DefaultExercise]()
    var exerciseNamesSearch = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.dataSource = self
        table.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        mappedValues = exerciseData.map { $0.value }.flatMap{$0}.sorted(by: { (ex1, ex2) -> Bool in
            return ex1.name < ex2.name
        })
        
        exerciseNamesSearch = mappedValues.map({ (exercise) -> String in
            return exercise.name
        }).sorted()
        
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x3478F6)
        tabBarController?.tabBar.tintColor = UIColor(netHex: 0x3478F6)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "catCell")!
        cell.textLabel?.text = isSearching ? filteredData[indexPath.row] : categories[indexPath.row]
        return cell
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filteredData.count : categories.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isSearching {
            for exrciese in mappedValues where exrciese.name == filteredData[indexPath.row] {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "singleExerciseViewControllerID") as! SingleExerciseViewController
                vc.selectedexercise = exrciese
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "exerciseID") as! ExersicesViewController
            vc.selectedCategory = categories[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func finishEditing() {
        searchBar.text = nil
        isSearching = false
        view.endEditing(true)
        table.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        finishEditing()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            finishEditing()
        } else {
            isSearching = true
            filteredData = Array(Set(searchText.isEmpty ? exerciseNamesSearch : exerciseNamesSearch.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.lowercased().contains(searchText.lowercased())
            }))).sorted()
            table.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
