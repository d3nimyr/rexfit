//
//  DefaultRoutines.swift
//  RexFit
//
//  Created by Hadevs on 05/10/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

let deafultRoutines: [RoutineWorkout] = [

    RoutineWorkout(name: "First Routine", defaultExercises: [
        DefaultExercise(description: "Sit on a bench holding a dumbbell in each hand with your palms facing inward:\n- Inhale and raise one arm at a time, turning the palm up\n- Raise your elbow to continue curling the dumbbell. \n\nNote: biomechanically, this exercise is excellent for emphasizing the biceps in all its actions (flexion and protraction of the arm and supination). \n\nThis exercise involves the brachioradialis, brachialis, biceps, anterior deltoids, and, to a lesser extent, the coracobrachial and upper pectorals.\nThree ways to curl dumbbells:\n1. Work both the biceps and brachials. \n2. Mainly work ihc brachioradialis. \n3. Mainly work the biceps.", image: [UIImage(named:"biceps_1_curls_1")!,UIImage(named:"biceps_1_curls_2")!,UIImage(named:"biceps_1_curls_3")!,UIImage(named:"biceps_1_curls_4")!], name: "Curls", category: "Biceps", type: .strength),
        DefaultExercise(description: "Sit on a bench. Hold a dumbbell with an underhand grip and rest your elbow on the inner side of your thigh:\n- inhale and curl the dumbbell;\n- exhale as you complete the movement.\n\nThis isolation exercise allows you to control the range, speed, and alignment of the movement. It works mainly the biceps, brachialis, and brachioradialis.", image: [UIImage(named:"biceps_2_concentration_curls_1")!], name: "Concentration curls", category: "Biceps", type: .strength)
        ])

]
