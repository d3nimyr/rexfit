//
//  WorkoutSettingTableViewCell.swift
//  RexFit
//
//  Created by Quaka on 19.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class WorkoutSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var settingTextField: UITextField!
    @IBOutlet weak var settingLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
