//
//  TextFieldTableViewCell.swift
//  RexFit
//
//  Created by Hadevs on 26/09/2017.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textField.delegate = self
        // Initialization code
    }
    
    var textChanged: ((String) -> Void)?
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let updatedText = textFieldText.replacingCharacters(in: range, with: string)
        
        if let completion = textChanged {
            completion(updatedText)
        }
        
        return true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
