//
//  DonePanel.swift
//  RexFit
//
//  Created by Hadevs on 19.07.17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit

class DonePanel: UIView {
    
    private var line: UIView!
    var doneButton: UIButton!
    var leftButton: UIButton!
    var rightButton: UIButton!
    var isNeedToHideArrows = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(netHex: 0xf7f7f7)
        addComponents()
    }
    
    private func addComponents() {
        addUpperLine()
        addDoneButton()
        addLeftButton()
        addRightButton()
    }
    
    func addLeftButton() {
        leftButton = UIButton()
        leftButton.translatesAutoresizingMaskIntoConstraints = false
        leftButton.isHidden = isNeedToHideArrows
        
        leftButton.setImage(#imageLiteral(resourceName: "left"), for: .normal)
        
        addSubview(leftButton)
        
        addConstraints(NSLayoutConstraint.contraints(withNewVisualFormat: "V:|-12-[b]-12-|", dict: ["b": leftButton]) + [NSLayoutConstraint.quadroAspect(on: leftButton)])
    }
    
    func addRightButton() {
        rightButton = UIButton()
        rightButton.translatesAutoresizingMaskIntoConstraints = false
        rightButton.isHidden = isNeedToHideArrows
        rightButton.setImage(#imageLiteral(resourceName: "right"), for: .normal)
        
        addSubview(rightButton)
        
        addConstraints(NSLayoutConstraint.contraints(withNewVisualFormat: "H:|-8-[b]-8-[r],V:|-12-[r]-12-|", dict: ["r": rightButton, "b": leftButton]) + [NSLayoutConstraint.quadroAspect(on: rightButton)])
    }
    
    private func addDoneButton() {
        doneButton = UIButton()
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 17.0)
        doneButton.setTitleColor(UIColor(netHex: 0x3478F6), for: .normal)
        doneButton.setTitle(NSLocalizedString("done", comment: ""), for: .normal)
        
        addSubview(doneButton)
        addConstraints(NSLayoutConstraint.contraints(withNewVisualFormat: "H:[d]-16-|,V:|-12-[d]-12-|", dict: ["d": doneButton]))
    }
    
    private func addUpperLine() {
        line = UIView()
        line.frame.size.height = 0.5
        line.frame.size.width = frame.width
        line.frame.origin = CGPoint(x: 0, y: 0)
        line.backgroundColor = .lightGray
        
        addSubview(line)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


extension NSLayoutConstraint {
    static func quadroAspect(on view: UIView) -> NSLayoutConstraint {
        return NSLayoutConstraint.init(item: view, attribute: .height, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
    }
    static func contraints(withNewVisualFormat vf: String, dict: [String: Any]) -> [NSLayoutConstraint] {
        let separatedArray = vf.split(separator: ",")
        switch separatedArray.count {
        case 1: return NSLayoutConstraint.constraints(withVisualFormat: "\(separatedArray[0])", options: [], metrics: nil, views: dict)
        case 2: return NSLayoutConstraint.constraints(withVisualFormat: "\(separatedArray[0])", options: [], metrics: nil, views: dict) + NSLayoutConstraint.constraints(withVisualFormat: "\(separatedArray[1])", options: [], metrics: nil, views: dict)
        default: return NSLayoutConstraint.constraints(withVisualFormat: "\(separatedArray[0])", options: [], metrics: nil, views: dict)
        }
    }
}
