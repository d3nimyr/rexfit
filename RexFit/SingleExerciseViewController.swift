//
//  SingleExerciseViewController.swift
//  RexFit
//
//  Created by d3nimyr on 10/07/17.
//  Copyright © 2017 Никита. All rights reserved.
//

import UIKit
import ImageSlideshow

class SingleExerciseViewController: UIViewController {
    
    @IBOutlet weak var singleExerciseText: UITextView!
    @IBOutlet weak var singleExerciseImageView: UIImageView!
    @IBOutlet var slideshow: ImageSlideshow!
    
    var selectedexercise: DefaultExercise!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedexercise.name
        singleExerciseText.text = selectedexercise.description
        slideShowSettings()
        navigationController?.navigationBar.tintColor = UIColor(netHex: 0x3478F6)
    }
    
    func loadPhotos(fromImageArray array: [UIImage]) -> [ImageSource] {
        return array.map({ (image) -> ImageSource in
            return ImageSource(image: image)
        })
    }
    
    func slideShowSettings() {
        let localSource = loadPhotos(fromImageArray: selectedexercise.image)
        slideshow.backgroundColor = UIColor.white
        
        slideshow.slideshowInterval = 10.0
        slideshow.pageControlPosition = PageControlPosition.underScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        slideshow.pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFit
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        slideshow.setImageInputs(localSource)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(SingleExerciseViewController.didTap))
        slideshow.addGestureRecognizer(recognizer)
    }
    
    func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
